package dam.androidisrael.remembrit.usecases.calendar

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.imageview.ShapeableImageView
import dam.androidisrael.remembrit.R
import dam.androidisrael.remembrit.databinding.ActivityCalendarBinding
import dam.androidisrael.remembrit.model.domain.Movie
import dam.androidisrael.remembrit.usecases.calendar.event.EventAdapter
import dam.androidisrael.remembrit.usecases.calendar.model.Event
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.MonthDay
import java.time.YearMonth
import java.time.format.TextStyle
import java.time.temporal.TemporalAmount
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseUser
import dam.androidisrael.remembrit.model.domain.SeasonEpisodeInfo
import dam.androidisrael.remembrit.model.session.UserSession
import dam.androidisrael.remembrit.provider.services.api.TMDBApiCall
import dam.androidisrael.remembrit.provider.services.firebase.FireB
import dam.androidisrael.remembrit.usecases.common.controls.TitleFragment
import dam.androidisrael.remembrit.usecases.explore.ExploreActivity
import dam.androidisrael.remembrit.usecases.main.MainActivity
import dam.androidisrael.remembrit.util.common.AsyncUtils
import dam.androidisrael.remembrit.util.common.MovieUtils
import dam.androidisrael.remembrit.util.extension.*
import okhttp3.Response
import java.util.concurrent.atomic.AtomicInteger


class CalendarActivity : AppCompatActivity() {
    companion object{
        const val NONE = "none"
    }
    private lateinit var eventDays: HashMap<LocalDate, ArrayList<Event>>
    private lateinit var binding: ActivityCalendarBinding
    private lateinit var _handler : Handler
    private var selectedDate: LocalDate = LocalDate.now()
    private var isCalendarVisible: Boolean = true
    private var _call = TMDBApiCall()
    private var events = ArrayList<Event>()
    private var numberOfEventsLoaded = AtomicInteger(0)
    private var totalNumberOfEvents = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityCalendarBinding.inflate(layoutInflater)
        setContentView(binding.root)
        _handler = Handler(Looper.getMainLooper())

        setUI()
    }

    private fun setUI() {
        var title: TitleFragment = TitleFragment.newInstance("Calendario")
        supportFragmentManager.beginTransaction().add(R.id.fragmentContainerView2, title).commit()
        var firebase = FireB()
        val userUID = UserSession.getUserUID(this)!!
        firebase.createFavs(userUID, this)
        firebase.getFavs(userUID){ list ->
            if (!list.isNull()){
                totalNumberOfEvents = list!!.size
                if(totalNumberOfEvents == 0) loadMonth(true)
                var hasTvShowsToLoad = list.any { it["type"] == "tv" }
                for (i in list){
                    var movie = Movie(i["id"].toString(), i["title"].toString(), "", "", LocalDate.ofEpochDay(i["release"] as Long), 0, 0, i["type"].toString())
                    if (movie.type == "movie"){
                        events.add(Event(LocalDate.ofEpochDay(i["release"] as Long), movie))
                        binding.tvInfoLoad.text = "${numberOfEventsLoaded.incrementAndGet()}/${totalNumberOfEvents}"
                    } else {
                        _call.getTv(movie.id.toInt(), _responseTvForSeasons)
                    }
                }
                events = events.filter { event -> event.day.year >= LocalDate.now().year - 1 } as ArrayList<Event>
                events.sortBy { event -> event.day.toEpochDay() }
                if (!hasTvShowsToLoad) {
                    loadMonth(true)
                }
            } else{
                loadMonth(true)
            }
        }
        initShimmer()
        setNavigation()
        var layoutManagerEvents:RecyclerView.LayoutManager = LinearLayoutManager(applicationContext)
        var layoutManager:RecyclerView.LayoutManager = GridLayoutManager(applicationContext, 7)
        binding.recyclerCalendar.layoutManager = layoutManager
        binding.recyclerCalendar.overScrollMode = View.OVER_SCROLL_NEVER
        binding.next.setOnClickListener {
            selectedDate = selectedDate.plusMonths(1)
            loadMonth(false)
        }
        binding.back.setOnClickListener {
            selectedDate = selectedDate.minusMonths(1)
            loadMonth(false)
        }
        binding.recyclerEvents.layoutManager = layoutManagerEvents
        //eventDays = getEventDays(events)
        binding.recyclerEvents.adapter = EventAdapter(ArrayList())
        binding.CalendarToList.setOnClickListener {
            if (isCalendarVisible) changeToList()
            else changeToCalendar()
            isCalendarVisible = binding.calendar.visibility == View.VISIBLE
        }
    }

    private fun initShimmer() {
        var calendarAdapter = CalendarAdapter(selectedDate, true){ view, day, month ->}
        binding.recyclerCalendar.adapter = calendarAdapter
    }

    private fun quitShimmer() {
        binding.shimmerCalendar.hideShimmer()
    }

    private fun setNavigation() {
        binding.bottomNavigationViewCalendar.selectedItemId = R.id.calendario
        binding.bottomNavigationViewCalendar.setOnNavigationItemSelectedListener(BottomNavigationView.OnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.inicio -> {
                    startActivity(Intent(applicationContext, MainActivity::class.java))
                    overridePendingTransition(0,0)
                    finish()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.explorar -> {
                    startActivity(Intent(applicationContext, ExploreActivity::class.java))
                    overridePendingTransition(0,0)
                    finish()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.calendario -> {
                    return@OnNavigationItemSelectedListener true
                }
                else -> return@OnNavigationItemSelectedListener false
            }
        })
    }

    private fun changeToCalendar() {
        binding.recyclerEvents.adapter = EventAdapter(ArrayList())
        binding.calendar.visibility = View.VISIBLE
        val params = binding.recyclerEvents.layoutParams as ConstraintLayout.LayoutParams
        params.setMargins(0, 0, 0, 0)
        binding.recyclerEvents.layoutParams = params
        binding.CalendarToList.setBackgroundResource(R.drawable.calendar_to_list)
    }

    private fun changeToList() {
        binding.recyclerEvents.adapter = CalendarEventAdapter(eventDays)
        binding.calendar.visibility = View.GONE
        val params = binding.recyclerEvents.layoutParams as ConstraintLayout.LayoutParams
        params.setMargins(0, 115, 0, 0)
        binding.recyclerEvents.layoutParams = params
        if (!eventDays.isNullOrEmpty()){
            binding.recyclerEvents.scrollToPosition((binding.recyclerEvents.adapter as CalendarEventAdapter).getItemPos(eventDays.filter { it.key >= LocalDate.now() }.keys.first()))
        }
        binding.CalendarToList.setBackgroundResource(R.drawable.list_to_calendar)
    }

    private fun getEventDays(events: ArrayList<Event>): HashMap<LocalDate, ArrayList<Event>> {
        return events.groupBy { it.day } as HashMap<LocalDate, ArrayList<Event>>
    }

    private fun loadMonth(initial:Boolean){
        binding.recyclerEvents.adapter = EventAdapter(ArrayList())
        AsyncUtils.executor.execute {
            var calendarAdapter = CalendarAdapter(selectedDate, false){ view, day, month ->
                var background = view.findViewById(R.id.calendarEvent) as ShapeableImageView
                var today = LocalDate.now()
                var textDay: TextView = view.findViewById(R.id.tvNumDay) as TextView
                if (today.dayOfMonth == day && today.month.value == month && selectedDate.year == today.year){
                    textDay.setTextColor(view.context.color(R.color.rose))
                }
                background.setOnClickListener{
                    var eventToday = eventDays[LocalDate.of(selectedDate.year, month, day)]
                    binding.recyclerEvents.adapter = EventAdapter(eventToday?:ArrayList())
                }
                if (eventDays.containsKey(LocalDate.of(selectedDate.year, month, day))){
                    var eventMarked = view.findViewById(R.id.eventMarked) as ShapeableImageView
                    eventMarked.visibility = View.VISIBLE
                }
            }
            var monthText = selectedDate.month.getDisplayName(TextStyle.FULL, Locale.getDefault())
            _handler.post {
                binding.tvMes.text =  "${monthText.first().uppercaseChar()}${monthText.subSequence(1,monthText.length)}, ${selectedDate.year}"
                binding.recyclerCalendar.adapter = calendarAdapter
                if (initial){
                    eventDays = getEventDays(events)
                    quitShimmer()
                }
            }
        }
    }

    // Response Functions

    private val _responseTvForSeasons = fun(hasWorked:Boolean, response: Response?){
        if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
        else{
            val responseString: String = response?.body()?.string() ?: ""
            var movie: Movie = MovieUtils.JsonTvToMovie(responseString)
            _handler.post {
                for (i in 1..movie.seasons){
                    _call.getTvSeason(movie.id.toInt(), i, _responseSeasonsInEvents(movie.id.toInt(), i, movie.seasons))
                }
            }
        }
    }

    private fun _responseSeasonsInEvents(tvId:Int, seasonNumber: Int, numberOfSeasons:Int) = fun(hasWorked:Boolean, response: Response?){
        if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
        else{
            val responseString: String = response?.body()?.string() ?: ""
            var info = SeasonEpisodeInfo(tvId.toString(), seasonNumber, 0)
            var episodes: ArrayList<Movie> = MovieUtils.JsonSeasonEpisodesToMovies(responseString, info)
            _handler.post {
                for (episode in episodes){
                    events.add(Event(episode.release, episode))
                }
                if(seasonNumber == numberOfSeasons){
                    binding.tvInfoLoad.text = "${numberOfEventsLoaded.incrementAndGet()}/${totalNumberOfEvents}"
                }
                events = events.filter { event -> event.day.year >= LocalDate.now().year - 1 } as ArrayList<Event>
                events.sortBy { event -> event.day.toEpochDay() }
                eventDays = getEventDays(events)
                loadMonth(true)
            }
        }
    }
}