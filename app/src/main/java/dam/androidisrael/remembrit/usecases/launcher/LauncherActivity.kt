package dam.androidisrael.remembrit.usecases.launcher

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import dam.androidisrael.remembrit.R
import dam.androidisrael.remembrit.usecases.login.LoginActivity

class LauncherActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)
        setTheme(R.style.Theme_Remembrit)
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }
}