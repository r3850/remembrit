package dam.androidisrael.remembrit.usecases.lists

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.auth.User
import dam.androidisrael.remembrit.R
import dam.androidisrael.remembrit.model.domain.ListFB
import dam.androidisrael.remembrit.model.domain.Movie
import dam.androidisrael.remembrit.model.session.UserSession
import dam.androidisrael.remembrit.provider.services.firebase.FireB
import dam.androidisrael.remembrit.usecases.detail.ListAdapter

class ListMoviesAdapter(var lists: ArrayList<ListFB>, var loading: Boolean, var context: Context, var callback: (list:ListFB)->Unit) : RecyclerView.Adapter<ListMoviesAdapter.ListViewHolder>() {
    class ListViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var title = view.findViewById(R.id.tvTitleList) as TextView
        var delete = view.findViewById(R.id.ivDelete) as ImageView
        fun bind(list: ListFB, loading: Boolean, context: Context, callback: (ListFB) -> Unit){
            var firebase = FireB()
            var uid = UserSession.getUserUID(context)
            if (!loading) {
                title.setOnClickListener { callback(list) }
                title.background = context.getDrawable(R.drawable.transparent_background)
                title.text = list.title
                val builder = AlertDialog.Builder(context)
                builder.setMessage("Estás seguro de querer borrar esta lista? ["+list.title+"]")
                    .setPositiveButton("Aceptar",
                        DialogInterface.OnClickListener { dialog, id ->
                            if(uid != null){
                                firebase.deleteFromListInfo(list, uid)
                                firebase.deleteList(list)
                                context.startActivity(Intent(context, ListsActivity::class.java))
                                (context as AppCompatActivity).overridePendingTransition(0,0)
                                (context as AppCompatActivity).finish()
                            }
                        })
                    .setNegativeButton("Cancelar",
                        DialogInterface.OnClickListener { dialog, id ->
                        })
                var dialog = builder.create()
                delete.setOnClickListener {
                    dialog.show()
                }
            }
        }
    }

    override fun onBindViewHolder(holder: ListMoviesAdapter.ListViewHolder, position: Int) {
        val item = lists[position]
        holder.bind(item, loading, context, callback)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListMoviesAdapter.ListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ListMoviesAdapter.ListViewHolder(
            layoutInflater.inflate(
                R.layout.list_detail,
                parent,
                false
            )
        )
    }
    override fun getItemCount(): Int {
        return lists.size
    }
}