package dam.androidisrael.remembrit.usecases.detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dam.androidisrael.remembrit.R
import dam.androidisrael.remembrit.model.domain.ListFB
import dam.androidisrael.remembrit.model.domain.Movie
import dam.androidisrael.remembrit.model.session.UserSession
import dam.androidisrael.remembrit.provider.services.firebase.FireB
import dam.androidisrael.remembrit.util.extension.alert
import dam.androidisrael.remembrit.util.extension.toast

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "movie"

/**
 * A simple [Fragment] subclass.
 * Use the [ListsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ListsFragment : DialogFragment() {
    // TODO: Rename and change types of parameters
    private var movie: Movie? = null
    private var firebase = FireB()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            movie = it.getSerializable(ARG_PARAM1) as Movie
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_lists, container, false)
        var btAdd = view.findViewById(R.id.btAdd) as Button
        var btDone = view.findViewById(R.id.btDone) as Button
        btDone.setOnClickListener {
            this.dismiss()
        }
        var etList = view.findViewById(R.id.etList) as EditText

        var rvLists = view.findViewById(R.id.rvLists) as RecyclerView
        rvLists.layoutManager = LinearLayoutManager(context)
        var uid = UserSession.getUserUID(context!!.applicationContext)
        getLists(uid, rvLists)

        btAdd.setOnClickListener {
            if (etList.text.toString() != ""){
                if (uid != null){
                    firebase.createListInfo(uid)
                    firebase.getListId(){
                        var list = ListFB(it, etList.text.toString(), false)
                        firebase.addToListInfo(list, uid)
                        firebase.createList(list)
                        getLists(uid, rvLists)
                        context!!.toast("Lista creada")
                    }
                }
            } else {
                context!!.toast("La lista debe tener un nombre")
            }
        }

        return view
    }

    private fun getLists(uid: String?, rvLists: RecyclerView) {
        if (uid != null){
            firebase.getListInfo(uid){
                if((it != null && it.isNotEmpty()) && movie != null){
                    var i = 0
                    var lists = ArrayList<ListFB>()
                    while (i < it.size){
                        var list = ListFB(it[i]["id"].toString(), it[i]["title"].toString(), false)
                        firebase.existsOnList(movie!!, list){
                            list.hasMovie = it
                            lists.add(list)
                            rvLists.adapter = ListAdapter(lists, movie!!)
                        }
                        i++
                    }
                }

            }
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ListsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(movie: Movie) =
            ListsFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_PARAM1, movie)
                }
            }
    }
}