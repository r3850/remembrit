package dam.androidisrael.remembrit.usecases.lists

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.common.api.Response
import dam.androidisrael.remembrit.R
import dam.androidisrael.remembrit.databinding.ActivityListsBinding
import dam.androidisrael.remembrit.model.domain.ListFB
import dam.androidisrael.remembrit.model.domain.Movie
import dam.androidisrael.remembrit.model.session.UserSession
import dam.androidisrael.remembrit.provider.services.api.TMDBApiCall
import dam.androidisrael.remembrit.provider.services.firebase.FireB
import dam.androidisrael.remembrit.usecases.common.controls.TitleFragment
import dam.androidisrael.remembrit.usecases.common.rows.MoviePosterAdapter
import dam.androidisrael.remembrit.usecases.detail.ListAdapter
import dam.androidisrael.remembrit.usecases.main.MainActivity
import dam.androidisrael.remembrit.util.common.MovieUtils
import dam.androidisrael.remembrit.util.extension.alert
import dam.androidisrael.remembrit.util.extension.toast
import java.time.LocalDate
import java.util.HashMap

class ListsActivity : AppCompatActivity() {
    private lateinit var _binding: ActivityListsBinding
    private var _call = TMDBApiCall()
    private lateinit var _handler : Handler
    private var moviesList = ArrayList<Movie>()
    private var size = 0
    private var prefix = "https://www.remembrit.it/"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityListsBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        _handler = Handler(Looper.getMainLooper())

        setUI()
    }

    private fun setUI() {
        supportActionBar?.hide()
        var uri: Uri? = intent.data
        var firebase = FireB()
        var uid = UserSession.getUserUID(this)
        _binding.ivShare.visibility = View.INVISIBLE
        if (uri == null){
            var title: TitleFragment = TitleFragment.newInstance("Listas")
            supportFragmentManager.beginTransaction().add(R.id.fragmentTitleLists, title).commit()
            _binding.ivBack.setOnClickListener {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }

            _binding.rvListsMovies.layoutManager = LinearLayoutManager(this)
            showLists(uid, firebase)
        }else {
            var title: TitleFragment = TitleFragment.newInstance("")
            supportFragmentManager.beginTransaction().add(R.id.fragmentTitleLists, title).commit()
            _binding.ivBack.setOnClickListener {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
            var path = uri.toString().removePrefix(prefix)
            var list = ListFB(path, "", false)
            firebase.listExists(list){
                if (it){
                    getMoviesList(firebase, list)
                } else{
                    alert("La lista no existe.")
                }
            }

        }


    }

    private fun getMoviesList(firebase: FireB, list: ListFB) {
        firebase.getMoviesOnList(list){ movies ->
            if((movies.isNotEmpty())) {
                _call.executeBatch(getThreadsIdMovies(movies)){
                    var inicio = System.currentTimeMillis()
                    while (moviesList.size != size){
                        if((System.currentTimeMillis() - inicio) >= 10000) break
                        Thread.sleep(25)
                    }
                    _handler.post {
                        _binding.rvListsMovies.layoutManager = GridLayoutManager(this, 3)
                        _binding.rvListsMovies.adapter = MoviePosterAdapter(moviesList, this)
                    }
                }
            }
        }
    }

    private fun showLists(uid: String?, firebase: FireB) {
        //_binding.rvListsMovies.adapter = ListMoviesAdapter(getLoading(), true,this){}
        if (uid != null){
            firebase.getLists(uid){
                if((it != null && it.isNotEmpty())) {
                    var i = 0
                    var lists = ArrayList<ListFB>()
                    while (i < it.size) {
                        var list = ListFB(it[i]["id"].toString(), it[i]["title"].toString(), false)
                        lists.add(list)
                        _binding.rvListsMovies.adapter = ListMoviesAdapter(lists, false,this){ listResult->
                            firebase.getMoviesOnList(listResult){ movies ->
                                if((movies.isNotEmpty())) {
                                    _call.executeBatch(getThreadsIdMovies(movies)){
                                        var inicio = System.currentTimeMillis()
                                        while (moviesList.size != size){
                                            if((System.currentTimeMillis() - inicio) >= 10000) break
                                            Thread.sleep(25)
                                        }
                                        _handler.post {
                                            _binding.ivShare.visibility = View.VISIBLE
                                            _binding.ivShare.setOnClickListener {
                                                var text = UserSession.getUserEmail(this)+" quiere que veas esta lista en Remembrit: "+ prefix+listResult.id
                                                var whatsapp = Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send?text=$text"))
                                                startActivity(whatsapp)
                                            }
                                            _binding.ivBack.setOnClickListener {
                                                startActivity(Intent(this, ListsActivity::class.java))
                                                overridePendingTransition(0, 0)
                                                finish()
                                            }
                                            _binding.rvListsMovies.layoutManager = GridLayoutManager(this, 3)
                                            _binding.rvListsMovies.adapter = MoviePosterAdapter(moviesList, this)
                                        }
                                    }
                                }
                            }
                        }
                        i++
                    }
                }
            }
        }
    }

    private fun getLoading(): ArrayList<ListFB> {
        var loading = ArrayList<ListFB>()
        for (i in 1..20) loading.add(ListFB("0", "", false))
        return loading
    }

    private fun getThreadsIdMovies(movies: List<HashMap<String, Any>>): ArrayList<Thread> {
        var threads = ArrayList<Thread>()
        moviesList.clear()
        var j = 0
        while (j < movies.size) {
            if(movies[j]["type"].toString() == "movie"){
                threads.add(_call.getThreadByMovieId(movies[j]["id"].toString(), _responseOnMoviesList()))
            } else {
                threads.add(_call.getThreadByTvId(movies[j]["id"].toString(), _responseTvOnMoviesList()))
            }
            j++
        }
        size = threads.size
        return threads
    }

    private fun _responseOnMoviesList(): (Boolean, okhttp3.Response?) -> Unit {
        return fun(hasWorked: Boolean, response: okhttp3.Response?) {
            if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
            else {
                val responseString: String = response?.body()?.string() ?: ""
                var movie: Movie = MovieUtils.JsonMovieToMovie(responseString)
                _handler.post { moviesList.add(movie) }
            }
        }
    }

    private fun _responseTvOnMoviesList(): (Boolean, okhttp3.Response?) -> Unit {
        return fun(hasWorked: Boolean, response: okhttp3.Response?) {
            if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
            else {
                val responseString: String = response?.body()?.string() ?: ""
                var movie: Movie = MovieUtils.JsonTvToMovie(responseString)
                _handler.post { moviesList.add(movie) }
            }
        }
    }
}