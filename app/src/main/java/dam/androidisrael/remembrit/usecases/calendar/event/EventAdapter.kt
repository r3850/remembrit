package dam.androidisrael.remembrit.usecases.calendar.event

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.imageview.ShapeableImageView
import dam.androidisrael.remembrit.R
import dam.androidisrael.remembrit.usecases.calendar.model.Event
import dam.androidisrael.remembrit.usecases.detail.DetailActivity

class EventAdapter(var events: ArrayList<Event>) : RecyclerView.Adapter<EventAdapter.EventViewHolder>() {
    class EventViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var title = view.findViewById(R.id.tvTitle) as TextView
        var desc = view.findViewById(R.id.tvDesc) as TextView
        var background = view.findViewById(R.id.calendarEvent) as ShapeableImageView
        var context = view.context
        fun bind(event: Event){
            var titleMovie: String = event.movie.title
            if (titleMovie.length > 30){
                titleMovie = titleMovie.substring(0, 29) + "..."
            }
            if (event.movie.type!="movie"){
                desc.text = context.getString(R.string.NewEpisode)
            }
            title.text = titleMovie
            background.setOnClickListener {
                val intent = Intent(context, DetailActivity::class.java).apply {
                    putExtra("movie", event.movie)
                }
                context.startActivity(intent)
            }
        }
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        val item = events[position]
        holder.bind(item)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return EventViewHolder(layoutInflater.inflate(R.layout.calendar_event, parent, false))
    }
    override fun getItemCount(): Int {
        return events.size
    }
}
