package dam.androidisrael.remembrit.usecases.login

import android.content.Intent
import android.graphics.Paint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import dam.androidisrael.remembrit.R
import dam.androidisrael.remembrit.databinding.ActivityLoginBinding
import dam.androidisrael.remembrit.databinding.ActivityMainBinding
import dam.androidisrael.remembrit.model.session.UserSession
import dam.androidisrael.remembrit.provider.services.firebase.FireB
import dam.androidisrael.remembrit.usecases.main.MainActivity
import dam.androidisrael.remembrit.util.extension.alert
import dam.androidisrael.remembrit.util.extension.isNull
import dam.androidisrael.remembrit.util.extension.toast

class LoginActivity : AppCompatActivity() {
    private lateinit var _binding: ActivityLoginBinding
    private var state: String = "login"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        setUI()
    }

    private fun setUI() {
        supportActionBar?.hide()
        _binding.tvChange.paintFlags = Paint.UNDERLINE_TEXT_FLAG
        iniciarSesionORegistrarse()
        _binding.tvChange.setOnClickListener {
            if (state == "login"){
                state = "register"
                _binding.tvChange.text = getString(R.string.OLogIn)
            } else if (state == "register"){
                state = "login"
                _binding.tvChange.text = getString(R.string.ORegister)
            }
            iniciarSesionORegistrarse()
        }
        if(UserSession.getUserUID(this).isNull() || UserSession.getUserUID(this) == ""){

        } else {
            startActivity(Intent(this.applicationContext, MainActivity::class.java))
            finish()
        }

    }

    fun iniciarSesionORegistrarse(){
        if (state == "login"){
            _binding.tvTitleLogIn.text = getString(R.string.log_in)
            _binding.btLogIn.text = getString(R.string.LogIn)
            _binding.etPassword2.visibility = View.GONE
            _binding.tvRepetir.visibility = View.GONE
            _binding.btLogIn.setOnClickListener{
                if (android.util.Patterns.EMAIL_ADDRESS.matcher(_binding.etEmail.text).matches() && _binding.etPassword.text.isNotEmpty()){
                    UserSession.logIn(_binding.etEmail.text.toString(), _binding.etPassword.text.toString(), this){
                        if (it.isSuccessful){
                            //alert(UserSession.getUserUID(this).toString())
                            startActivity(Intent(this.applicationContext, MainActivity::class.java))
                            finish()
                        }else{
                            toast("Usuario o contraseña incorrectos")
                        }
                    }
                }else{
                    toast("Datos incorrectos o no rellenados")
                }
            }
        } else if(state == "register"){
            _binding.etPassword2.visibility = View.VISIBLE
            _binding.tvRepetir.visibility = View.VISIBLE
            _binding.tvTitleLogIn.text = getString(R.string.register)
            _binding.btLogIn.text = getString(R.string.Register_text)
            _binding.btLogIn.setOnClickListener{
                if ((android.util.Patterns.EMAIL_ADDRESS.matcher(_binding.etEmail.text.toString()).matches() && _binding.etPassword.text.toString().isNotEmpty()) && (_binding.etPassword.text.toString() == _binding.etPassword2.text.toString())){
                    if (_binding.etPassword.text.toString().length >= 6) {
                        UserSession.register(_binding.etEmail.text.toString(), _binding.etPassword.text.toString(), this){
                            if (it.isSuccessful){
                                toast("Usuario creado, inicia sesión", Toast.LENGTH_LONG)
                                startActivity(Intent(this.applicationContext, LoginActivity::class.java))
                                finish()
                            }else{
                                //alert(it.exception.toString())
                                if (it.exception != null){
                                    toast("El usuario no se ha podido crear. "+it.exception?.message ?: "", Toast.LENGTH_LONG)
                                } else{
                                    toast("El usuario no se ha podido crear")
                                }
                            }
                        }
                    } else {
                        toast("La contraseña debe contener 6 carácteres como mínimo")
                    }
                }else{
                    toast("Datos incorrectos o no rellenados, revisa que las contraseñas coincidan")
                }
            }
        }
    }
}