package dam.androidisrael.remembrit.usecases.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dam.androidisrael.remembrit.R
import dam.androidisrael.remembrit.model.domain.Movie
import dam.androidisrael.remembrit.usecases.common.rows.MoviePosterAdapter
import java.util.concurrent.ConcurrentHashMap

class CategoryMoviesAdapter(var categories: ConcurrentHashMap<String, ArrayList<Movie>>, var context: Context): RecyclerView.Adapter<CategoryMoviesAdapter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvTitleCategory = view.findViewById<TextView>(R.id.tvTitleCategory)
        var rvMovies = view.findViewById<RecyclerView>(R.id.rvMovies)
        fun bind(title: String, movies: ArrayList<Movie>, context: Context){
            var linearLayoutManager = LinearLayoutManager(context)
            linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
            rvMovies.layoutManager = linearLayoutManager
            tvTitleCategory.text = title
            rvMovies.adapter = MoviePosterAdapter(movies, context)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.main_movie_category, parent, false))
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val key = categories.keys.toList()[position]
        val movies = categories[key] as ArrayList<Movie>
        holder.bind(key, movies, context)
    }
}