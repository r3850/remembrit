package dam.androidisrael.remembrit.usecases.calendar.model

import dam.androidisrael.remembrit.model.domain.Movie
import java.io.Serializable
import java.time.LocalDate

data class Event(val day: LocalDate, val movie: Movie):Serializable