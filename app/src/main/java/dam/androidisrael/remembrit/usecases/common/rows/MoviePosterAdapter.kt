package dam.androidisrael.remembrit.usecases.common.rows

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import dam.androidisrael.remembrit.R
import dam.androidisrael.remembrit.model.domain.Movie
import dam.androidisrael.remembrit.provider.services.api.TMDBApiCall
import dam.androidisrael.remembrit.usecases.detail.DetailActivity
import dam.androidisrael.remembrit.util.extension.load
import dam.androidisrael.remembrit.util.extension.toast

class MoviePosterAdapter(var movies: MutableList<Movie>, var context: Context) :
    RecyclerView.Adapter<MoviePosterAdapter.MovieViewHolder>() {

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val item = movies[position]
        holder.bind(item, context)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return MovieViewHolder(layoutInflater.inflate(R.layout.movie_poster, parent, false))
    }
    override fun getItemCount(): Int {
        return movies.size
    }

    class MovieViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        private val poster = view.findViewById(R.id.calendarEvent) as ImageView
        fun bind(movie: Movie, context: Context){
            if (movie.poster == "") {
                poster.background = context.getDrawable(R.drawable.loading_background)
            } else{
                poster.load(TMDBApiCall.getAbsoluteURL(movie.poster))
            }
            if(movie.id != ""){
                itemView.setOnClickListener(View.OnClickListener {
                    val intent = Intent(context, DetailActivity::class.java).apply {
                        putExtra("movie", movie)
                    }
                    context.startActivity(intent)
                })
            }
        }
    }
}