package dam.androidisrael.remembrit.usecases.detail

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.imageview.ShapeableImageView
import dam.androidisrael.remembrit.R
import dam.androidisrael.remembrit.model.domain.Movie
import dam.androidisrael.remembrit.provider.services.api.TMDBApiCall
import dam.androidisrael.remembrit.util.extension.load

class EpisodeAdapter(var movies: MutableList<Movie>,var loading: Boolean, var context: Context): RecyclerView.Adapter<EpisodeAdapter.EpisodeViewHolder>() {

    class EpisodeViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var poster = view.findViewById(R.id.ivPosterEpisode) as ImageView
        var title = view.findViewById(R.id.tvDetailTitle) as TextView
        var info = view.findViewById(R.id.tvDetailInfo) as TextView
        var constraintLayout = view.findViewById(R.id.clEpisode) as ConstraintLayout
        fun bind(episode: Movie, loading: Boolean, context: Context){
            if (!loading){
                title.text = episode.title
                info.text = episode.release.toString()
                constraintLayout.setOnClickListener {
                    val intent = Intent(context, DetailActivity::class.java).apply {
                        putExtra("movie", episode)
                    }
                    context.startActivity(intent)
                }
                poster.load(TMDBApiCall.getAbsoluteURL(episode.poster))
            } else {
                var drawable = context.getDrawable(R.drawable.loading_background)
                poster.background = drawable
                info.background = drawable
                title.background = drawable
            }
        }
    }

    override fun onBindViewHolder(holder: EpisodeAdapter.EpisodeViewHolder, position: Int) {
        val item = movies[position]
        holder.bind(item, loading, context)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodeAdapter.EpisodeViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return EpisodeAdapter.EpisodeViewHolder(
            layoutInflater.inflate(
                R.layout.detail_episode,
                parent,
                false
            )
        )
    }
    override fun getItemCount(): Int {
        return movies.size
    }

}