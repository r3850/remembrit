package dam.androidisrael.remembrit.usecases.main

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import dam.androidisrael.remembrit.R
import dam.androidisrael.remembrit.databinding.ActivityMainBinding
import dam.androidisrael.remembrit.model.domain.Movie
import dam.androidisrael.remembrit.model.session.UserSession
import dam.androidisrael.remembrit.provider.services.api.TMDBApiCall
import dam.androidisrael.remembrit.provider.services.firebase.FireB
import dam.androidisrael.remembrit.usecases.calendar.CalendarActivity
import dam.androidisrael.remembrit.usecases.calendar.model.Event
import dam.androidisrael.remembrit.usecases.common.controls.TitleFragment
import dam.androidisrael.remembrit.usecases.common.rows.MoviePosterAdapter
import dam.androidisrael.remembrit.usecases.detail.DetailActivity
import dam.androidisrael.remembrit.usecases.explore.ExploreActivity
import dam.androidisrael.remembrit.usecases.lists.ListsActivity
import dam.androidisrael.remembrit.usecases.login.LoginActivity
import dam.androidisrael.remembrit.util.common.AsyncUtils
import dam.androidisrael.remembrit.util.common.MovieUtils
import dam.androidisrael.remembrit.util.extension.alert
import dam.androidisrael.remembrit.util.extension.isNull
import dam.androidisrael.remembrit.util.extension.load
import dam.androidisrael.remembrit.util.extension.toast
import okhttp3.Response
import java.time.LocalDate
import java.util.concurrent.ConcurrentHashMap
import kotlin.concurrent.thread
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    private lateinit var _binding: ActivityMainBinding
    private lateinit var _handler : Handler
    private val _call = TMDBApiCall()
    private var _movies: MutableList<Movie> = ArrayList()
    private var _categories: ConcurrentHashMap<String,ArrayList<Movie>> = ConcurrentHashMap()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        _handler = Handler(Looper.getMainLooper())
        _binding.sidebarNav.getHeaderView(0).findViewById<TextView>(R.id.userName).text = "USER: ${UserSession.getUserEmail(this)}"
        _binding.sidebarButton.setOnClickListener {
            _binding.drawerLayout.open()
        }
        var context = this
        _binding.sidebarNav.menu.findItem(R.id.cerrar_sesion).setOnMenuItemClickListener {
            UserSession.logOut(context)
            startActivity(Intent(applicationContext, LoginActivity::class.java))
            context.finish()
            true
        }
        _binding.sidebarNav.menu.findItem(R.id.listas).setOnMenuItemClickListener {
            startActivity(Intent(this, ListsActivity::class.java))
            context.finish()
            true
        }

        if (hasInternet()){
            setUI()
        }
    }

    private fun setUI() {
        var title: TitleFragment = TitleFragment.newInstance("Inicio")
        supportFragmentManager.beginTransaction().add(R.id.fragmentContainerView3, title).commit()
        _binding.bottomNavigationView.selectedItemId = R.id.inicio
        _binding.bottomNavigationView.setOnNavigationItemSelectedListener(BottomNavigationView.OnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.inicio -> {
                    return@OnNavigationItemSelectedListener true
                }
                R.id.explorar -> {
                    /*UserSession.logOut(this)
                    startActivity(Intent(applicationContext, LoginActivity::class.java))
                    finish()*/
                    startActivity(Intent(applicationContext, ExploreActivity::class.java))
                    overridePendingTransition(0,0)
                    finish()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.calendario -> {
                    startActivity(Intent(applicationContext, CalendarActivity::class.java))
                    overridePendingTransition(0,0)
                    finish()
                    return@OnNavigationItemSelectedListener true
                }
                else -> return@OnNavigationItemSelectedListener false
            }
        })
        var linearLayoutManager = LinearLayoutManager(this)
        _binding.recyclerView.layoutManager = linearLayoutManager
        _binding.recyclerView.adapter = CategoryMoviesAdapter(getLoadingCategories(),this)
        var categories: ArrayList<Thread> = getCategories()
        _call.getMovie(634649, _responseIntoMainMovie())//Spiderman: No way home
        _call.executeBatch(categories, _updateCategories)
    }

    private fun getLoadingCategories(): ConcurrentHashMap<String, ArrayList<Movie>> {
        var categories: ConcurrentHashMap<String, ArrayList<Movie>> = ConcurrentHashMap()
        var loadingImages: ArrayList<Movie> = ArrayList()
        var movie: Movie = Movie("", "", "", "", LocalDate.of(1,1,1))
        for (i in 1..3) loadingImages.add(movie)
        for (i in 1..3) categories[""+i] = loadingImages
        return categories
    }

    private fun getCategories(): ArrayList<Thread> {
        var categories = ArrayList<Thread>()
        //Para no sobrecargar la memoria usar como MUCHO 20 categorías.
        //Cada una contiene 20 películas con su información y su imagen(cargada por el glide)
        //En total 400 películas.
        categories.add(_call.getThreadByCollection(645, _responseCollectionInHashList("Colección 007")))
        categories.add(_call.getThreadByCollection(10, _responseCollectionInHashList("Colección Star Wars")))
        categories.add(_call.getThreadByCollection(9485, _responseCollectionInHashList("Colección Fast & Furious")))
        categories.add(_call.getThreadByDiscover("movie", "", "", LocalDate.now().toString(), 1, _responseInHashList("Novedades en películas")))
        categories.add(_call.getThreadByDiscover("tv", "", "", LocalDate.now().toString(), 1, _responseTvInHashList("Novedades en series")))
        categories.add(_call.getThreadByDiscover("movie", "", "", "", 1, _responseInHashList("Películas más populares")))
        categories.add(_call.getThreadByDiscover("tv", "", "", "", 1, _responseTvInHashList("Series más populares")))
        categories.add(_call.getThreadByNowPlaying(_responseInHashList("Películas en cines")))
        categories.add(_call.getThreadByUpcoming(_responseInHashList("Próximas películas")))
        categories.add(_call.getThreadByOnTheAir(_responseTvInHashList("Series en emisión")))
        categories.add(_call.getThreadByAiringToday(_responseTvInHashList("Series con capítulos hoy")))
        return categories
    }


    private fun hasInternet() : Boolean{
        val requestPermissionLauncher =
            registerForActivityResult(ActivityResultContracts.RequestPermission())
            { isGranted: Boolean ->
                if (isGranted) {
                    toast("G", Toast.LENGTH_LONG)
                } else {
                    toast("R", Toast.LENGTH_LONG)
                }
            }
        when (PackageManager.PERMISSION_GRANTED) {
            ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) -> {
                return true
            } else -> {
            requestPermissionLauncher.launch(Manifest.permission.INTERNET)
        }
        }
        return false
    }

    // Response Functions

    private val _responseInAlert = fun(hasWorked:Boolean, response:Response?){
        if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
        else{
            val responseString: String = response?.body()?.string() ?: ""
            var movie: Movie = MovieUtils.JsonMovieToMovie(responseString)
            _handler.post { alert(movie.title) }
        }
    }
    private val _responseInList = fun(hasWorked:Boolean, response:Response?){
        if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
        else {
            val responseString: String = response?.body()?.string() ?: ""
            var movieList = MovieUtils.JsonToMovies(responseString)
            _handler.post {
                for (movie in movieList) addMovie(movie)
                updateMovies()
            }
        }
    }

    private fun _responseInHashList(title: String): (Boolean, Response?) -> Unit {
        return fun(hasWorked: Boolean, response: Response?) {
            if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
            else {
                val responseString: String = response?.body()?.string() ?: ""
                var movieList = MovieUtils.JsonToMovies(responseString)
                _handler.post {
                    var movies: ArrayList<Movie> = ArrayList()
                    for (movie in movieList) movies.add(movie)
                    addCategoryMovies(title, movies)
                }
            }
        }
    }

    private fun _responseCollectionInHashList(title: String): (Boolean, Response?) -> Unit {
        return fun(hasWorked: Boolean, response: Response?) {
            if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
            else {
                val responseString: String = response?.body()?.string() ?: ""
                var movieList = MovieUtils.JsonCollectionToMovies(responseString)
                _handler.post {
                    var movies: ArrayList<Movie> = ArrayList()
                    for (movie in movieList) movies.add(movie)
                    addCategoryMovies(title, movies)
                }
            }
        }
    }

    private fun _responseTvInHashList(title: String): (Boolean, Response?) -> Unit {
        return fun(hasWorked: Boolean, response: Response?) {
            if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
            else {
                val responseString: String = response?.body()?.string() ?: ""
                var movieList = MovieUtils.JsonTvsToMovies(responseString)
                _handler.post {
                    var movies: ArrayList<Movie> = ArrayList()
                    for (movie in movieList) movies.add(movie)
                    addCategoryMovies(title, movies)
                }
            }
        }
    }

    private fun _responseMultipleInHashList(): (Boolean, Response?, String) -> Unit {
        return fun(hasWorked: Boolean, response: Response?, extraInfo:String) {
            if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
            else {
                val responseString: String = response?.body()?.string() ?: ""
                var movieList = MovieUtils.JsonToMovies(responseString)
                _handler.post {
                    var movies: ArrayList<Movie> = ArrayList()
                    for (movie in movieList) movies.add(movie)
                    addCategoryMovies(extraInfo, movies)
                }
            }
        }
    }

    private fun _responseIntoMainMovie(): (Boolean, Response?) -> Unit {
        return fun(hasWorked: Boolean, response: Response?) {
            if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
            else{
                val responseString: String = response?.body()?.string() ?: ""
                var movie: Movie = MovieUtils.JsonMovieToMovie(responseString)
                _handler.post {
                    _binding.ivMainPoster.load(TMDBApiCall.getAbsoluteMainURL(movie.poster))
                    _binding.ivMainPoster.setOnClickListener(View.OnClickListener {
                        val intent = Intent(this, DetailActivity::class.java).apply {
                            putExtra("movie", movie)
                        }
                        startActivity(intent)
                    })
                    _binding.tvMainTitle2.text = movie.title
                    _binding.tvMainTitle2.background = getDrawable(R.drawable.transparent_background)
                }
            }
        }
    }

    private val _updateCategories = fun(size: Int){
        var inicio = System.currentTimeMillis()
        while (_categories.size != size){
            if((System.currentTimeMillis() - inicio) >= 10000) break
            Thread.sleep(25)
        }
        _handler.post {
            updateCategories()
            quitShimmer()
        }
    }

    //Useful Functions

    private fun quitShimmer() {
        _binding.ivMainPoster.background = getDrawable(R.drawable.transparent_background)
        _binding.tvMainTitle2.background = getDrawable(R.drawable.transparent_background)
        _binding.shimmer.hideShimmer()
    }

    private fun addMovie(movie:Movie){
        _movies.add(movie)
    }
    private fun addCategoryMovies(title:String, movies:ArrayList<Movie>){
        _categories[title] = movies
    }
    private fun updateCategories(){
        this._binding.recyclerView.adapter = CategoryMoviesAdapter(_categories, this)
    }
    private fun updateMovies(){
        this._binding.recyclerView.adapter = MoviePosterAdapter(_movies, this)
    }
    private fun updateMovies(movie:Movie){
        addMovie(movie)
        updateMovies()
    }
    private fun updateMovies(movies:MutableList<Movie>){
        _movies = movies
        updateMovies()
    }
}
