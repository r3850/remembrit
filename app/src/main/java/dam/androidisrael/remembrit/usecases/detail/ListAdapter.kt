package dam.androidisrael.remembrit.usecases.detail

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import dam.androidisrael.remembrit.R
import dam.androidisrael.remembrit.model.domain.ListFB
import dam.androidisrael.remembrit.model.domain.Movie
import dam.androidisrael.remembrit.provider.services.api.TMDBApiCall
import dam.androidisrael.remembrit.provider.services.firebase.DataListFB
import dam.androidisrael.remembrit.provider.services.firebase.FireB
import dam.androidisrael.remembrit.util.extension.load

class ListAdapter(var lists: ArrayList<ListFB>, var movie: Movie): RecyclerView.Adapter<ListAdapter.ListViewHolder>() {
    class ListViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var cbList: CheckBox = view.findViewById(R.id.cbList) as CheckBox
        fun bind(list: ListFB, movie: Movie){
            cbList.text = list.title
            cbList.isChecked = list.hasMovie
            var firebase = FireB()
            cbList.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked){
                    firebase.addToList(movie, list)
                } else{
                    firebase.deleteOfList(movie, list)
                }
            }
        }
    }

    override fun onBindViewHolder(holder: ListAdapter.ListViewHolder, position: Int) {
        val item = lists[position]
        holder.bind(item, movie)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListAdapter.ListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ListAdapter.ListViewHolder(
            layoutInflater.inflate(
                R.layout.list_card,
                parent,
                false
            )
        )
    }
    override fun getItemCount(): Int {
        return lists.size
    }
}