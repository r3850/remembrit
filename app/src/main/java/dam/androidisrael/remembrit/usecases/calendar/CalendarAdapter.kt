package dam.androidisrael.remembrit.usecases.calendar

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.imageview.ShapeableImageView
import dam.androidisrael.remembrit.R
import dam.androidisrael.remembrit.usecases.calendar.CalendarAdapter.*
import dam.androidisrael.remembrit.util.extension.color
import dam.androidisrael.remembrit.util.extension.font
import java.time.LocalDate
import java.time.YearMonth

class CalendarAdapter(var selectedDate: LocalDate,var loading:Boolean, var onExternalBind:(View, Int, Int) -> Unit) : RecyclerView.Adapter<CalendarViewHolder>(){
    private var daysOfMonth = daysInMonthArray(selectedDate)

    class CalendarViewHolder(itemView: View, var onExternalBind: (View, Int, Int) -> Unit) : RecyclerView.ViewHolder(itemView) {
        private var textDay: TextView = itemView.findViewById(R.id.tvNumDay) as TextView
        private var background = itemView.findViewById(R.id.calendarEvent) as ShapeableImageView
        fun bind(day: String, month: Int){
            if (day != CalendarActivity.NONE) {
                onExternalBind.invoke(itemView, day.toInt(), month)

                textDay.text = day
                textDay.typeface = itemView.context.font(R.font.lexend_deca)
            } else {
                background.visibility = View.INVISIBLE
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalendarViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.calendar_cell, parent, false)
        if (loading) {
            view.findViewById<ShapeableImageView>(R.id.calendarEvent).background = parent.context.getDrawable(R.drawable.loading_background)
        }
        return CalendarViewHolder(view, onExternalBind)
    }

    override fun onBindViewHolder(holder: CalendarViewHolder, position: Int) {
        holder.bind(daysOfMonth[position], selectedDate.month.value)
    }

    override fun getItemCount(): Int {
        return daysOfMonth.size
    }

    private fun daysInMonthArray(selectedDate: LocalDate): ArrayList<String> {
        val daysInMonth: ArrayList<String> = ArrayList()
        val yearMonth: YearMonth = YearMonth.from(selectedDate)
        val daysInMonthNum: Int = yearMonth.lengthOfMonth()
        val dayOfWeek = selectedDate.withDayOfMonth(1).dayOfWeek.value - 1
        var i = 1
        while (i <= 42){
            if(i <= dayOfWeek || i > daysInMonthNum + dayOfWeek) daysInMonth.add(CalendarActivity.NONE)
            else daysInMonth.add("${i-dayOfWeek}")
            i++
        }
        return daysInMonth
    }

}