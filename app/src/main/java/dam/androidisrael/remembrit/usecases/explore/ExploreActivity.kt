package dam.androidisrael.remembrit.usecases.explore

import android.content.Intent
import android.os.*
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import android.widget.SearchView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import dam.androidisrael.remembrit.R
import dam.androidisrael.remembrit.databinding.ActivityExploreBinding
import dam.androidisrael.remembrit.model.domain.Movie
import dam.androidisrael.remembrit.provider.services.api.TMDBApiCall
import dam.androidisrael.remembrit.usecases.calendar.CalendarActivity
import dam.androidisrael.remembrit.usecases.common.controls.TitleFragment
import dam.androidisrael.remembrit.usecases.common.rows.MoviePosterAdapter
import dam.androidisrael.remembrit.usecases.main.MainActivity
import dam.androidisrael.remembrit.util.common.MovieUtils
import dam.androidisrael.remembrit.util.extension.alert
import dam.androidisrael.remembrit.util.extension.isNull
import okhttp3.Response
import java.time.LocalDate
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ExploreActivity : AppCompatActivity() {
    private lateinit var _binding: ActivityExploreBinding
    private val _call = TMDBApiCall()
    private lateinit var _handler : Handler
    private var _movies: MutableList<Movie> = Collections.synchronizedList(ArrayList<Movie>())
    private var currentSearch: String = ""
    private var currentPage: Int = 1
    private var state: String = ""
    private lateinit var savedRecyclerLayoutState: Parcelable
    private var selection = Selection()

    class Selection(
        var year: String = "",
        var genre: String = "",
        var content: String = ""
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityExploreBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        _handler = Handler(Looper.getMainLooper())
        setUI()
    }

    private fun setUI() {
        supportActionBar?.hide()
        var title: TitleFragment = TitleFragment.newInstance("Explorar")
        var titleVacio: TitleFragment = TitleFragment.newInstance("")
        configureFilters()
        supportFragmentManager.beginTransaction().add(R.id.fragmentTitle, title).commit()
        _binding.bottomNavigationViewExplore.selectedItemId = R.id.inicio
        _binding.bottomNavigationViewExplore.setOnNavigationItemSelectedListener(BottomNavigationView.OnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.inicio -> {
                    startActivity(Intent(applicationContext, MainActivity::class.java))
                    overridePendingTransition(0,0)
                    finish()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.explorar -> {
                    return@OnNavigationItemSelectedListener true
                }
                R.id.calendario -> {
                    startActivity(Intent(applicationContext, CalendarActivity::class.java))
                    overridePendingTransition(0,0)
                    finish()
                    return@OnNavigationItemSelectedListener true
                }
                else -> return@OnNavigationItemSelectedListener false
            }
        })
        var layoutManager = GridLayoutManager(this, 3)
        _binding.rvExplore.layoutManager = layoutManager
        _binding.svMovies.setOnSearchClickListener {
            var params = _binding.svMovies.layoutParams
            params.width = 900
            supportFragmentManager.beginTransaction().replace(R.id.fragmentTitle, titleVacio).commit()
            _binding.svMovies.layoutParams = params
        }
        _binding.svMovies.setOnCloseListener(SearchView.OnCloseListener {
            var params = _binding.svMovies.layoutParams
            params.width = 100
            supportFragmentManager.beginTransaction().replace(R.id.fragmentTitle, title).commit()
            _binding.svMovies.layoutParams = params
            false
        })
        _binding.rvExplore.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {
                    if (currentPage != 0 && currentPage <= 10){
                        savedRecyclerLayoutState = _binding.rvExplore.layoutManager!!.onSaveInstanceState()!!
                        when (state){
                            "discover" -> {
                                currentPage++
                                filterSearch(false)
                            }
                            "search"->{
                                if (currentSearch.isNotEmpty()){
                                    currentPage++
                                    _call.getMovieBySearch(currentSearch, currentPage, _responseInRecyclerView(false))
                                }
                            }
                        }
                    }
                }
            }
        })
        _binding.svMovies.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (!query.isNull()) {
                    currentSearch = query!!
                    currentPage = 1
                    _binding.spGenre2.setSelection(0)
                    _binding.spYear2.setSelection(0)
                    _binding.spContent2.setSelection(0)
                    savedRecyclerLayoutState = _binding.rvExplore.layoutManager!!.onSaveInstanceState()!!
                    initShimmer()
                    state = "search"
                    _binding.rvExplore.adapter = MoviePosterAdapter(getLoading(), applicationContext)
                    _call.getMovieBySearch(query, _responseInRecyclerView(true))
                }
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }
        })
        initShimmer()
        _binding.rvExplore.adapter = MoviePosterAdapter(getLoading(), applicationContext)
        state = "discover"
        _call.getMovieByDiscover(LocalDate.now().year.toString(), "", currentPage, _responseInRecyclerViewDiscover(true))
    }

    fun filterSearch(init: Boolean){
        when (selection.content) {
            "movie" -> {
                _call.getMovieByDiscover(selection.year, selection.genre, currentPage, _responseInRecyclerViewDiscover(init))
            }
            "tv" -> {
                _call.getTvByDiscover(selection.year, selection.genre, currentPage, _responseTvInRecyclerViewDiscover(init))
            }
            else ->{
                _call.getMovieByDiscover(selection.year, selection.genre, currentPage, _responseInRecyclerViewDiscover(init))
            }
        }
    }

    private fun getLoading(): ArrayList<Movie> {
        var loadingImages: ArrayList<Movie> = ArrayList()
        var movie = Movie("", "", "", "", LocalDate.of(1,1,1))
        for (i in 1..12) loadingImages.add(movie)
        return loadingImages
    }

    private fun _responseInRecyclerView(init: Boolean): (Boolean, Response?) -> Unit {
        return fun(hasWorked: Boolean, response: Response?) {
            if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
            else {
                val responseString: String = response?.body()?.string() ?: ""
                var movieList = MovieUtils.JsonToMovies(responseString)
                _handler.post {
                    if (init){
                        var movies: ArrayList<Movie> = ArrayList()
                        for (movie in movieList) movies.add(movie)
                        _movies = movies
                    } else{
                        for (movie in movieList) _movies.add(movie)
                    }
                    _call.getTvBySearch(currentSearch, currentPage, _responseTvInRecyclerView(init))
                    if(!init){
                        _binding.rvExplore.layoutManager?.onRestoreInstanceState(savedRecyclerLayoutState)
                    }
                }
            }
        }
    }
    private fun _responseTvInRecyclerView(init: Boolean): (Boolean, Response?) -> Unit {
        return fun(hasWorked: Boolean, response: Response?) {
            if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
            else {
                val responseString: String = response?.body()?.string() ?: ""
                var movieList = MovieUtils.JsonTvsToMovies(responseString)
                _handler.post {
                    for (movie in movieList) _movies.add(movie)
                    sortCorrectly()
                    _binding.rvExplore.adapter = MoviePosterAdapter(_movies, this)
                    quitShimmer()
                    if(!init){
                        _binding.rvExplore.layoutManager?.onRestoreInstanceState(savedRecyclerLayoutState)
                    }
                }
            }
        }
    }

    private fun _responseInRecyclerViewDiscover(init: Boolean): (Boolean, Response?) -> Unit {
        return fun(hasWorked: Boolean, response: Response?) {
            if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
            else {
                val responseString: String = response?.body()?.string() ?: ""
                var movieList = MovieUtils.JsonToMovies(responseString)
                _handler.post {
                    if (init){
                        var movies: ArrayList<Movie> = ArrayList()
                        for (movie in movieList) movies.add(movie)
                        _movies = movies
                    } else{
                        for (movie in movieList) _movies.add(movie)
                    }
                    quitShimmer()
                    _binding.rvExplore.adapter = MoviePosterAdapter(_movies, this)
                    //_call.getTvBySearch(currentSearch, currentPage, _responseTvInRecyclerView(init))
                    if(!init){
                        _binding.rvExplore.layoutManager?.onRestoreInstanceState(savedRecyclerLayoutState)
                    }
                }
            }
        }
    }

    private fun _responseTvInRecyclerViewDiscover(init: Boolean): (Boolean, Response?) -> Unit {
        return fun(hasWorked: Boolean, response: Response?) {
            if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
            else {
                val responseString: String = response?.body()?.string() ?: ""
                var movieList = MovieUtils.JsonTvsToMovies(responseString)
                _handler.post {
                    if (init){
                        var movies: ArrayList<Movie> = ArrayList()
                        for (movie in movieList) movies.add(movie)
                        _movies = movies
                    } else{
                        for (movie in movieList) _movies.add(movie)
                    }
                    quitShimmer()
                    _binding.rvExplore.adapter = MoviePosterAdapter(_movies, this)
                    //_call.getTvBySearch(currentSearch, currentPage, _responseTvInRecyclerView(init))
                    if(!init){
                        _binding.rvExplore.layoutManager?.onRestoreInstanceState(savedRecyclerLayoutState)
                    }
                }
            }
        }
    }

    //Useful Functions

    private fun sortCorrectly() {
        var sortposition = (currentPage - 1) * 40
        var maintainMovies = _movies.filterIndexed{ index, movie -> index < sortposition}
        var sortMovies = _movies.filterIndexed { index, movie -> index >= sortposition}
        sortMovies = sortMovies.sortedWith(compareBy<Movie>{ it.title.lowercase(Locale.getDefault()) != currentSearch.lowercase(Locale.getDefault()) }.thenByDescending { it.release })
        _movies = ArrayList()
        _movies.addAll(maintainMovies)
        _movies.addAll(sortMovies)
    }

    private fun initShimmer() {
        _binding.shimmerExplore.showShimmer(true)
    }

    private fun quitShimmer() {
        _binding.shimmerExplore.hideShimmer()
    }

    private fun configureFilters() {
        var years = MutableList<String>(61) {
            if (it != 0){
                "" + (it + LocalDate.now().year - 50)
            } else{
                ""
            }
        }

        var genresMovieHash: HashMap<String, String> = getMovieGenres()

        var genresMovie = MutableList<String>(genresMovieHash.size){""}
        for((i, genre) in genresMovieHash.keys.withIndex()){
            genresMovie[i] = genre
        }

        var genresTvHash: HashMap<String, String> = getTvGenres()

        var genresTv = MutableList<String>(genresTvHash.size){""}
        for((i, genre) in genresTvHash.keys.withIndex()){
            genresTv[i] = genre
        }

        var contents: HashMap<String, String> = getContents()

        var contentTypes = MutableList<String>(contents.size){""}
        for((i, content) in contents.keys.withIndex()){
            contentTypes[i] = content
        }

        val adapterContent: ArrayAdapter<String> = ArrayAdapter<String>(
            this.applicationContext,
            R.layout.dropdown_selected_item,
            contentTypes
        )
        adapterContent.setDropDownViewResource(R.layout.dropdown_item)

        val adapterYear: ArrayAdapter<String> = ArrayAdapter<String>(
            this.applicationContext,
            R.layout.dropdown_selected_item,
            years
        )
        adapterYear.setDropDownViewResource(R.layout.dropdown_item)
        val adapterMovieGenre: ArrayAdapter<String> = ArrayAdapter<String>(
            this.applicationContext,
            R.layout.dropdown_selected_item,
            genresMovie
        )
        adapterMovieGenre.setDropDownViewResource(R.layout.dropdown_item)
        val adapterTvGenre: ArrayAdapter<String> = ArrayAdapter<String>(
            this.applicationContext,
            R.layout.dropdown_selected_item,
            genresTv
        )
        adapterTvGenre.setDropDownViewResource(R.layout.dropdown_item)
        var spinnerYear = _binding.spYear2
        var spinnerGenre = _binding.spGenre2
        var spinnerContent = _binding.spContent2
        spinnerGenre.adapter = adapterMovieGenre
        spinnerContent.adapter = adapterContent
        spinnerYear.adapter = adapterYear
        spinnerYear.onItemSelectedListener = object: AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                currentPage = 1
                selection.year = adapterYear.getItem(position) ?: ""
                initShimmer()
                _binding.rvExplore.adapter = MoviePosterAdapter(getLoading(), applicationContext)
                filterSearch(true)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

            }

        }
        spinnerGenre.onItemSelectedListener = object: AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                currentPage = 1
                if(selection.content == "movie"){
                    selection.genre = genresMovieHash[adapterMovieGenre.getItem(position)] ?: ""
                } else if (selection.content == "tv"){
                    selection.genre = genresTvHash[adapterTvGenre.getItem(position)] ?: ""
                }
                initShimmer()
                _binding.rvExplore.adapter = MoviePosterAdapter(getLoading(), applicationContext)
                filterSearch(true)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

            }
        }
        spinnerContent.onItemSelectedListener = object: AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                currentPage = 1
                selection.content = contents[contentTypes[position]] ?: ""
                if(selection.content == "movie"){
                    selection.genre = ""
                    spinnerGenre.adapter = adapterMovieGenre
                } else if (selection.content == "tv"){
                    selection.genre = ""
                    spinnerGenre.adapter = adapterTvGenre
                }
                initShimmer()
                _binding.rvExplore.adapter = MoviePosterAdapter(getLoading(), applicationContext)
                filterSearch(true)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

            }
        }
        var thisYear = adapterYear.getPosition(LocalDate.now().year.toString())
        spinnerYear.setSelection(thisYear)
        var initialContent = adapterContent.getPosition("Películas")
        spinnerContent.setSelection(initialContent)
    }

    private fun getContents(): HashMap<String, String> {
        var content = HashMap<String, String>()
        content.put("","")
        content.put("Series","tv")
        content.put("Películas","movie")
        return content
    }

    private fun getMovieGenres(): HashMap<String, String> {
        var genres = HashMap<String, String>()
        genres.put("", "")
        genres.put("Acción","28")
        genres.put("Aventura","12")
        genres.put("Animación","16")
        genres.put("Comedia","35")
        genres.put("Crimen","80")
        genres.put("Documental","99")
        genres.put("Drama","18")
        genres.put("Familia","10751")
        genres.put("Fantasía","14")
        genres.put("Historia","36")
        genres.put("Terror","27")
        genres.put("Misterio","9648")
        genres.put("Romance","10749")
        genres.put("Ciencia ficción","878")
        genres.put("Suspense","53")
        genres.put("Bélica","10752")
        genres.put("Western","37")
        return genres
    }

    private fun getTvGenres(): HashMap<String, String> {
        var genres = HashMap<String, String>()
        genres.put("", "")
        genres.put("Animación","16")
        genres.put("Comedia","35")
        genres.put("Acción","10759")
        genres.put("Crimen","80")
        genres.put("Documental","99")
        genres.put("Drama","18")
        genres.put("Familia","10751")
        genres.put("Infantiles","10762")
        genres.put("Misterio","9648")
        genres.put("Noticias","10763")
        genres.put("Ciencia Ficción","10765")
        genres.put("Western","37")
        return genres
    }
}