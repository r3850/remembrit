package dam.androidisrael.remembrit.usecases.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.CompoundButton
import dam.androidisrael.remembrit.R
import dam.androidisrael.remembrit.databinding.ActivityCalendarBinding
import dam.androidisrael.remembrit.databinding.ActivityDetailBinding
import dam.androidisrael.remembrit.model.domain.Movie
import dam.androidisrael.remembrit.provider.services.api.TMDBApiCall
import dam.androidisrael.remembrit.usecases.common.controls.TitleFragment
import dam.androidisrael.remembrit.util.common.MovieUtils
import dam.androidisrael.remembrit.util.extension.alert
import dam.androidisrael.remembrit.util.extension.load
import dam.androidisrael.remembrit.util.extension.toast
import okhttp3.Response

import android.graphics.drawable.Drawable
import android.view.View
import androidx.fragment.app.ListFragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import dam.androidisrael.remembrit.model.domain.SeasonEpisodeInfo
import dam.androidisrael.remembrit.model.session.UserSession
import dam.androidisrael.remembrit.provider.services.firebase.FireB
import dam.androidisrael.remembrit.usecases.calendar.event.EventAdapter
import dam.androidisrael.remembrit.usecases.common.rows.MoviePosterAdapter
import dam.androidisrael.remembrit.util.common.AsyncUtils
import dam.androidisrael.remembrit.util.extension.isNull
import java.time.LocalDate
import java.util.*
import kotlin.collections.ArrayList


class DetailActivity : AppCompatActivity() {
    private lateinit var _handler : Handler
    private val _call = TMDBApiCall()
    private lateinit var movieExtraInfo: Movie
    private lateinit var mainMovie: Movie
    private lateinit var binding: ActivityDetailBinding
    private var seasonsEpisodes: MutableList<Movie> = Collections.synchronizedList(ArrayList<Movie>())
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        _handler = Handler(Looper.getMainLooper())

        setUI()
    }

    private fun setUI() {
        supportActionBar?.hide()
        var firebase = FireB()
        var title: TitleFragment = TitleFragment.newInstance("")
        supportFragmentManager.beginTransaction().add(R.id.fragmentContainerView, title).commit()
        mainMovie = intent.getSerializableExtra("movie") as Movie
        var userUID = UserSession.getUserUID(this)!!
        firebase.createFavs(userUID, this)
        firebase.existsOnFav(mainMovie, userUID){ exists ->
            binding.tbSeguir.isChecked = exists
        }
        binding.tvSeasons.visibility = View.INVISIBLE
        binding.tbSeguir.visibility = View.INVISIBLE
        binding.ivLists.visibility = View.INVISIBLE
        when (mainMovie.type){
            "movie"->{
                binding.tvSeasons.visibility = View.VISIBLE
                binding.tbSeguir.visibility = View.VISIBLE
                binding.ivLists.visibility = View.VISIBLE
                _call.getMovie(Integer.parseInt(mainMovie.id), _responseInExtraInfo)
            }
            "tv"->{
                binding.tvSeasons.visibility = View.VISIBLE
                binding.tbSeguir.visibility = View.VISIBLE
                binding.ivLists.visibility = View.VISIBLE
                binding.rvSeasonsEpisodes.layoutManager = GridLayoutManager(this, 3)
                binding.rvSeasonsEpisodes.adapter = MoviePosterAdapter(getLoadingEpisodes(9), this)
                _call.getTv(Integer.parseInt(mainMovie.id), _responseTvInExtraInfo)
            }
            "season"->{
                binding.rvSeasonsEpisodes.layoutManager = LinearLayoutManager(this)
                binding.rvSeasonsEpisodes.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
                binding.rvSeasonsEpisodes.adapter = EpisodeAdapter(getLoadingEpisodes(5), true, this)
                _call.getTvSeason(Integer.parseInt(mainMovie.tvInfo!!.serieId), mainMovie.tvInfo!!.seasonNumber, _responseTvSeasonInExtraInfo(mainMovie.tvInfo!!.seasonNumber))
            }
            else->{
                _call.getTvSeasonEpisode(Integer.parseInt(mainMovie.tvInfo!!.serieId), mainMovie.tvInfo!!.seasonNumber, mainMovie.tvInfo!!.episodeNumber, _responseTvSeasonEpisodeInExtraInfo(mainMovie.tvInfo!!.seasonNumber, mainMovie.tvInfo!!.episodeNumber))
            }
        }
        if (binding.ivLists.visibility == View.VISIBLE){
            binding.ivLists.setOnClickListener {
                var lists: ListsFragment = ListsFragment.newInstance(mainMovie)
                lists.show(supportFragmentManager, "LISTS")
            }
        }
        binding.ivPoster.load(TMDBApiCall.getAbsoluteMainURL(mainMovie.poster))
        binding.tvMainTitle.text = mainMovie.title
        binding.tvYear.text = mainMovie.release.year.toString()
        if (mainMovie.description.isNullOrEmpty()){
            binding.tvDescripcion.text = getString(R.string.noDescription)
        }else binding.tvDescripcion.text = mainMovie.description
        binding.tbSeguir.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                binding.tbSeguir.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_checked, 0, 0)
                if(mainMovie.type == "tv" || mainMovie.type == "movie"){
                    firebase.addToFav(mainMovie, userUID)
                }
            } else {
                binding.tbSeguir.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_plus, 0, 0)
                firebase.deleteOfFav(mainMovie, userUID)
            }
        }
    }

    private fun getLoadingEpisodes(numberOfLoading: Int): MutableList<Movie> {
        var load = ArrayList<Movie>()
        var movie = Movie("", "", "", "", LocalDate.of(1,1,1), 0)
        for (i in 1..numberOfLoading) load.add(movie)
        return load
    }

    // Response Functions

    private fun _responseSeasonInRecyclerView(seasonNumber: Int): (Boolean, Response?) -> Unit {
        return fun(hasWorked:Boolean, response: Response?){
            if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
            else{
                val responseString: String = response?.body()?.string() ?: ""
                var info = SeasonEpisodeInfo(mainMovie.id, seasonNumber, 0)
                var season: Movie = MovieUtils.JsonSeasonToMovie(responseString, info)
                _handler.post {
                    season.id = mainMovie.id
                    seasonsEpisodes.add(season)
                    seasonsEpisodes.sortBy { it.tvInfo!!.seasonNumber }
                    binding.rvSeasonsEpisodes.adapter = MoviePosterAdapter(seasonsEpisodes, this)
                    quitShimmer()
                }
            }
        }
    }

    private fun _responseSeasonEpisodeInRecyclerView(seasonNumber: Int, episodeNumber:Int): (Boolean, Response?) -> Unit {
        return fun(hasWorked:Boolean, response: Response?){
            if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
            else{
                val responseString: String = response?.body()?.string() ?: ""
                var info = SeasonEpisodeInfo(mainMovie.id, seasonNumber, episodeNumber)
                var episode: Movie = MovieUtils.JsonSeasonEpisodeToMovie(responseString, info)
                _handler.post {
                    episode.id = mainMovie.id
                    seasonsEpisodes.add(episode)
                    seasonsEpisodes.sortBy { it.tvInfo!!.episodeNumber }
                    binding.rvSeasonsEpisodes.adapter = EpisodeAdapter(seasonsEpisodes, false, this)
                    quitShimmer()
                }
            }
        }
    }

    private val _responseInExtraInfo = fun(hasWorked:Boolean, response: Response?){
        if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
        else{
            val responseString: String = response?.body()?.string() ?: ""
            var movie: Movie = MovieUtils.JsonMovieToMovie(responseString)
            _handler.post {
                movieExtraInfo = movie
                updateExtraInfo()
            }
        }
    }

    private val _responseTvInExtraInfo = fun(hasWorked:Boolean, response: Response?){
        if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
        else{
            val responseString: String = response?.body()?.string() ?: ""
            var movie: Movie = MovieUtils.JsonTvToMovie(responseString)
            _handler.post {
                movieExtraInfo = movie
                updateExtraInfo()
            }
        }
    }

    private fun _responseTvSeasonInExtraInfo(seasonNumber: Int) = fun(hasWorked:Boolean, response: Response?){
        if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
        else{
            val responseString: String = response?.body()?.string() ?: ""
            var info = SeasonEpisodeInfo(mainMovie.tvInfo!!.serieId, seasonNumber, 0)
            var movie: Movie = MovieUtils.JsonSeasonToMovie(responseString, info)
            _handler.post {
                movieExtraInfo = movie
                updateExtraInfo()
            }
        }
    }

    private fun _responseTvSeasonEpisodeInExtraInfo(seasonNumber: Int, episodeNumber:Int) = fun(hasWorked:Boolean, response: Response?){
        if (!hasWorked) _handler.post { alert("ERROR: No internet connection.") }
        else{
            val responseString: String = response?.body()?.string() ?: ""
            var info = SeasonEpisodeInfo(mainMovie.tvInfo!!.serieId, seasonNumber, episodeNumber)
            var movie: Movie = MovieUtils.JsonSeasonEpisodeToMovie(responseString, info)
            _handler.post {
                movieExtraInfo = movie
                updateExtraInfo()
            }
        }
    }

    // Useful functions

    private fun updateExtraInfo() {
        if (movieExtraInfo.runtime != 0){
            binding.tvSeasons.text = convertToHours(movieExtraInfo.runtime)
        } else binding.tvSeasons.text = ""
        if(mainMovie.description == "") {
            if (movieExtraInfo.description.isNullOrEmpty()){
                binding.tvDescripcion.text = getString(R.string.noDescription)
            }else binding.tvDescripcion.text = movieExtraInfo.description
        }
        if(mainMovie.poster == "") binding.ivPoster.load(TMDBApiCall.getAbsoluteMainURL(movieExtraInfo.poster))
        if(movieExtraInfo.seasons!=0){
            var s = ""
            if (movieExtraInfo.seasons > 1) s = "s"
            binding.tvSeasons.text = "${movieExtraInfo.seasons} temporada$s"
            if(mainMovie.type == "tv"){
                for (i in 1..movieExtraInfo.seasons){
                    _call.getTvSeason(mainMovie.id.toInt(), i, _responseSeasonInRecyclerView(i))
                }
            }
            if(mainMovie.type == "season"){
                for (i in 1..movieExtraInfo.seasons){
                    _call.getTvSeasonEpisode(mainMovie.tvInfo!!.serieId.toInt(), mainMovie.tvInfo!!.seasonNumber, i, _responseSeasonEpisodeInRecyclerView(mainMovie.tvInfo!!.seasonNumber, i))
                }
            }
        }else{
            quitShimmer()
        }
    }

    private fun quitShimmer() {
        binding.tvSeasons.background = getDrawable(R.drawable.transparent_background)
        binding.lbSinopsis.background = getDrawable(R.drawable.transparent_background)
        binding.lbSinopsis.text = getString(R.string.Sinopsis)
        binding.tvMainTitle.background = getDrawable(R.drawable.transparent_background)
        binding.tvYear.background = getDrawable(R.drawable.transparent_background)
        binding.tvDescripcion.background = getDrawable(R.drawable.transparent_background)
        binding.shimmerDetail.hideShimmer()
    }

    private fun convertToHours(runtime: Int): String {
        var textHours: String = (runtime / 60).toString()
        var textMinutes: String = (runtime % 60).toString()
        textHours = "${textHours}h ${textMinutes}m"
        return textHours
    }
}