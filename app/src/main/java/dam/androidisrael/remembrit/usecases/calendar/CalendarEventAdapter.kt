package dam.androidisrael.remembrit.usecases.calendar

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dam.androidisrael.remembrit.R
import dam.androidisrael.remembrit.usecases.calendar.event.EventAdapter
import dam.androidisrael.remembrit.usecases.calendar.model.Event
import java.time.LocalDate
import java.time.format.TextStyle
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class CalendarEventAdapter(var eventDays: HashMap<LocalDate, ArrayList<Event>>) : RecyclerView.Adapter<CalendarEventAdapter.EventViewHolder>() {
    class EventViewHolder(var view: View) : RecyclerView.ViewHolder(view){
        var title = view.findViewById(R.id.tvDay) as TextView
        var recyclerView = view.findViewById(R.id.recyclerEventsOnDay) as RecyclerView
        fun bind(events: ArrayList<Event>){
            if (events.size > 0){
                var dayString = events[0].day.dayOfWeek.getDisplayName(TextStyle.SHORT, Locale.getDefault())
                title.text = dayString.first().uppercaseChar() + "" + dayString.substring(1, dayString.length) + " " + events[0].day.dayOfMonth + " de " + events[0].day.month.getDisplayName(TextStyle.FULL, Locale.getDefault()) + ", " + events[0].day.year.toString()
                recyclerView.layoutManager = LinearLayoutManager(view.context)
                recyclerView.adapter = EventAdapter(events)
            }
        }
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        val item = eventDays.values.filterIndexed { index, arrayList ->
            index == position
        }
        holder.bind(item[0])
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return EventViewHolder(layoutInflater.inflate(R.layout.calendar_event_day, parent, false))
    }

    fun getItemPos(key: LocalDate): Int {
        val item = eventDays.keys.forEachIndexed { index, list ->
           if (list == key){
               return index
           }
        }
        return -1
    }

    override fun getItemCount(): Int {
        return eventDays.size
    }
}
