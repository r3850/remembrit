package dam.androidisrael.remembrit.provider.services.api

import okhttp3.*
import java.io.IOException

class ApiCall(
    private var requestString: String,
    private var callback: (Boolean, Response?) -> Unit,
) : Runnable {
    private val client = OkHttpClient()

    override fun run() {
        val request = Request.Builder()
            .url(requestString)
            .build()

        client.newCall(request).enqueue(object: Callback{
            override fun onFailure(call: Call, e: IOException) {
                callback(false, null)
            }
            override fun onResponse(call: Call, response: Response) {
                callback(true, response)
            }
        })
    }
}