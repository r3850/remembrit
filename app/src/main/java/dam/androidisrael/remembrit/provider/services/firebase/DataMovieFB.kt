package dam.androidisrael.remembrit.provider.services.firebase

import java.time.LocalDate

data class DataMovieFB(var id: String, val title: String, var release: Long, var type: String)