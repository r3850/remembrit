package dam.androidisrael.remembrit.provider.services.firebase

import android.content.Context
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import dam.androidisrael.remembrit.model.domain.ListFB
import dam.androidisrael.remembrit.model.domain.Movie
import dam.androidisrael.remembrit.util.common.AsyncUtils
import dam.androidisrael.remembrit.util.extension.alert
import java.time.LocalDate
import java.time.temporal.TemporalField
import kotlin.random.Random

class FireB {
    private var firebase = FirebaseFirestore.getInstance()
    private var FAV = "favorites"
    private var LIST_INFO = "list_info"
    private var GLOBAL_INFO = "global_info"
    private var LISTS = "lists"
    fun logIn(email:String, password:String, onCompleteListener: (Task<AuthResult>)->Unit){
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password).addOnCompleteListener {
            onCompleteListener(it)
        }
    }

    fun register(email:String, password:String, onCompleteListener: (Task<AuthResult>)->Unit){
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password).addOnCompleteListener {
            onCompleteListener(it)
        }
    }

    fun logOut(){
        FirebaseAuth.getInstance().signOut()
    }

    fun createFavs(uid:String, context: Context){
        firebase.collection(FAV).document(uid).get().addOnCompleteListener {
            if (it.isSuccessful) {
                val document = it.result
                if (document != null && !document.exists()) {
                    firebase.collection(FAV).document(uid).set(hashMapOf(
                        FAV to arrayListOf<DataMovieFB>()
                    ))
                }
            }
        }
    }

    fun addToFav(movie: Movie, uid:String){
        var dataMovie = DataMovieFB(movie.id, movie.title, movie.release.toEpochDay(), movie.type)
        firebase.collection(FAV).document(uid).update(FAV, FieldValue.arrayUnion(dataMovie))
    }

    fun deleteOfFav(movie: Movie, uid:String){
        var dataMovie = DataMovieFB(movie.id, movie.title, movie.release.toEpochDay(), movie.type)
        firebase.collection(FAV).document(uid).update(FAV, FieldValue.arrayRemove(dataMovie))
    }

    fun existsOnFav(movie: Movie, uid:String, onCompleteListener: (Boolean) -> Unit){
        firebase.collection(FAV).document(uid).get().addOnCompleteListener { result ->
            if (result.isSuccessful){
                var list = result.result.get(FAV) as List<HashMap<String, Any>>
                onCompleteListener(list.any { it["id"].toString() == movie.id })
            }
        }
    }

    fun getFavs(uid:String, onCompleteListener: (List<HashMap<String, Any>>?) -> Unit){
        firebase.collection(FAV).document(uid).get().addOnCompleteListener { result ->
            var list: List<HashMap<String, Any>>? = null
            if (result.isSuccessful){
                list = result.result.get(FAV) as List<HashMap<String, Any>>?
            }
            onCompleteListener(list)
        }
    }

    fun getRandomFav(uid:String, type: String, onCompleteListener: (HashMap<String, Any>?) -> Unit){
        firebase.collection(FAV).document(uid).get().addOnCompleteListener { task ->
            if (task.isSuccessful){
                var list = task.result.get(FAV) as List<HashMap<String, Any>>?
                list!!.filter { it["type"]==type }
                onCompleteListener(list[Random.nextInt(0, list.size-1)])
            }
        }
    }

    fun createListInfo(uid:String){
        firebase.collection(LIST_INFO).document(uid).get().addOnCompleteListener {
            if (it.isSuccessful) {
                val document = it.result
                if (document != null && !document.exists()) {
                    firebase.collection(LIST_INFO).document(uid).set(hashMapOf(
                        LIST_INFO to arrayListOf<DataListFB>()
                    ))
                }
            }
        }
    }

    fun getListInfo(uid:String, onCompleteListener: (List<HashMap<String, Any>>?) -> Unit){
        firebase.collection(LIST_INFO).document(uid).get().addOnCompleteListener {
            if (it.isSuccessful) {
                var list = it.result.get(LIST_INFO) as List<HashMap<String, Any>>?
                onCompleteListener(list)
            }
        }
    }

    fun addToListInfo(list: ListFB, uid:String){
        var dataList = DataListFB(list.id, list.title)
        firebase.collection(LIST_INFO).document(uid).update(LIST_INFO, FieldValue.arrayUnion(dataList))
    }

    fun deleteFromListInfo(list: ListFB, uid:String){
        var dataList = DataListFB(list.id, list.title)
        firebase.collection(LIST_INFO).document(uid).update(LIST_INFO, FieldValue.arrayRemove(dataList))
    }

    fun deleteList(list: ListFB){
        firebase.collection(LISTS).document(list.id).get().addOnCompleteListener {
            if (it.isSuccessful) {
                val document = it.result
                if (document != null && document.exists()) {
                    firebase.collection(LISTS).document(list.id).delete()
                }
            }
        }
    }

    fun createList(list: ListFB){
        firebase.collection(LISTS).document(list.id).get().addOnCompleteListener {
            if (it.isSuccessful) {
                val document = it.result
                if (document != null && !document.exists()) {
                    firebase.collection(LISTS).document(list.id).set(hashMapOf(
                        LISTS to arrayListOf<DataMovieFB>()
                    ))
                }
            }
        }
    }

    fun getLists(uid:String, onCompleteListener: (List<HashMap<String, Any>>?) -> Unit){
        firebase.collection(LIST_INFO).document(uid).get().addOnCompleteListener { result ->
            var list: List<HashMap<String, Any>>? = null
            if (result.isSuccessful){
                list = result.result.get(LIST_INFO) as List<HashMap<String, Any>>?
            }
            onCompleteListener(list)
        }
    }

    fun addToList(movie: Movie, list: ListFB){
        var dataMovie = DataMovieFB(movie.id, movie.title, movie.release.toEpochDay(), movie.type)
        firebase.collection(LISTS).document(list.id).update(LISTS, FieldValue.arrayUnion(dataMovie))
    }

    fun deleteOfList(movie: Movie, list: ListFB){
        var dataMovie = DataMovieFB(movie.id, movie.title, movie.release.toEpochDay(), movie.type)
        firebase.collection(LISTS).document(list.id).update(LISTS, FieldValue.arrayRemove(dataMovie))
    }

    fun existsOnList(movie: Movie, list: ListFB, onCompleteListener: (Boolean) -> Unit){
        firebase.collection(LISTS).document(list.id).get().addOnCompleteListener { result ->
            if (result.isSuccessful){
                var list = result.result.get(LISTS) as List<HashMap<String, Any>>
                onCompleteListener(list.any { it["id"].toString() == movie.id })
            }
        }
    }

    fun listExists(list: ListFB, onCompleteListener: (Boolean) -> Unit){
        firebase.collection(LISTS).document(list.id).get().addOnCompleteListener { result ->
            if (result.isSuccessful){
                var document = result.result
                onCompleteListener(document != null && document.exists())
            }
        }
    }

    fun getMoviesOnList(list: ListFB, onCompleteListener: (List<HashMap<String, Any>>) -> Unit){
        firebase.collection(LISTS).document(list.id).get().addOnCompleteListener { result ->
            if (result.isSuccessful){
                var list = result.result.get(LISTS) as List<HashMap<String, Any>>
                onCompleteListener(list)
            }
        }
    }

    fun getListId(onCompleteListener: (String) -> Unit){
        firebase.collection(GLOBAL_INFO).document(GLOBAL_INFO).get().addOnCompleteListener { result ->
            if (result.isSuccessful){
                var id = result.result.get("id") as String
                addOneListId(id)
                onCompleteListener(id)
            }
        }
    }

    private fun addOneListId(id: String){
        var idPlus = (id.toLong() + 1).toString()
        firebase.collection(GLOBAL_INFO).document(GLOBAL_INFO).update("id", idPlus)
    }

}