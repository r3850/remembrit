package dam.androidisrael.remembrit.provider.services.api

import okhttp3.*
import java.io.IOException

class ApiCallExtraInfo(
    private var requestString: String,
    private var callback: (Boolean, Response?, String) -> Unit,
    private var extraInfo:String
) : Runnable {
    private val client = OkHttpClient()

    override fun run() {
        val request = Request.Builder()
            .url(requestString)
            .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                callback(false, null, extraInfo)
            }
            override fun onResponse(call: Call, response: Response) {
                callback(true, response, extraInfo)
            }
        })
    }
}