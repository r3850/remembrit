package dam.androidisrael.remembrit.provider.services.api

import dam.androidisrael.remembrit.model.domain.Movie
import dam.androidisrael.remembrit.model.domain.SeasonEpisodeInfo
import dam.androidisrael.remembrit.util.common.AsyncUtils.Companion.executor
import okhttp3.Response
import java.time.LocalDate
import java.util.concurrent.*
import kotlin.concurrent.thread


class TMDBApiCall {
    private val _tmdbUrl = "https://api.themoviedb.org/3"
    private val _apiKey = "api_key=99b22ba38ae2e3910243443a54223a09"
    private var _language = "es-ES"
    private var _region = "ES"

    fun getMovie(movie:Int, callback:(Boolean, Response?) -> Unit){
        executor.execute(Thread(ApiCall("$_tmdbUrl/movie/$movie?$_apiKey&language=$_language&region=$_region", callback)))
    }

    fun getThreadByMovieId(movie:String, callback:(Boolean, Response?) -> Unit): Thread{
        return Thread(ApiCall("$_tmdbUrl/movie/$movie?$_apiKey&language=$_language&region=$_region", callback))
    }

    fun getThreadByTvId(tv:String, callback:(Boolean, Response?) -> Unit): Thread{
        return Thread(ApiCall("$_tmdbUrl/tv/$tv?$_apiKey&language=$_language&region=$_region", callback))
    }

    fun getTv(tv:Int, callback:(Boolean, Response?) -> Unit){
        executor.execute(Thread(ApiCall("$_tmdbUrl/tv/$tv?$_apiKey&language=$_language&region=$_region", callback)))
    }

    fun getTvSeason(tv:Int, season:Int, callback:(Boolean, Response?) -> Unit){
        executor.execute(Thread(ApiCall("$_tmdbUrl/tv/$tv/season/$season?$_apiKey&language=$_language&region=$_region", callback)))
    }

    fun getTvSeasonEpisode(tv:Int, season:Int, episode: Int, callback:(Boolean, Response?) -> Unit){
        executor.execute(Thread(ApiCall("$_tmdbUrl/tv/$tv/season/$season/episode/$episode?$_apiKey&language=$_language&region=$_region", callback)))
    }

    fun getMovieBySearch(search:String, callback:(Boolean, Response?) -> Unit){
        executor.execute(Thread(ApiCall("$_tmdbUrl/search/movie?$_apiKey&language=$_language&query=$search&region=$_region", callback)))
    }

    fun getMovieBySearch(search:String, page: Int, callback:(Boolean, Response?) -> Unit){
        executor.execute(Thread(ApiCall("$_tmdbUrl/search/movie?$_apiKey&language=$_language&query=$search&region=$_region&page=$page", callback)))
    }

    fun getMovieByDiscover(year: String, genre: String, page: Int, callback:(Boolean, Response?) -> Unit){
        var request = "$_tmdbUrl/discover/movie?$_apiKey&language=$_language&region=$_region&page=$page&sort_by=popularity.desc"
        if (year != "") request += "&year=$year"
        if (genre != "") request += "&with_genres=$genre"
        executor.execute(Thread(ApiCall(request, callback)))
    }

    fun getTvByDiscover(year: String, genre: String, page: Int, callback:(Boolean, Response?) -> Unit){
        var request = "$_tmdbUrl/discover/tv?$_apiKey&language=$_language&region=$_region&page=$page&sort_by=popularity.desc"
        if (year != "") request += "&year=$year"
        if (genre != "") request += "&with_genres=$genre"
        executor.execute(Thread(ApiCall(request, callback)))
    }

    fun getThreadByDiscover(type: String, year: String, genre: String, fromDate: String, page: Int, callback:(Boolean, Response?) -> Unit): Thread{
        var request = "$_tmdbUrl/discover/$type?$_apiKey&language=$_language&region=$_region&page=$page&sort_by=popularity.desc"
        if (genre != "") request += "&with_genres=$genre"
        if(type == "movie"){
            if (fromDate != "") request += "&release_date.gte=$fromDate"
            if (year != "") request += "&year=$year"
        } else if (type == "tv"){
            if (fromDate != "") request += "&air_date.gte=$fromDate"
            if (year != "") request += "&first_air_date_year=$year"
        }
        return Thread(ApiCall(request, callback))
    }

    fun getThreadByCollection(id: Int, callback:(Boolean, Response?) -> Unit): Thread{
        var request = "$_tmdbUrl/collection/$id?$_apiKey&language=$_language&region=$_region"
        return Thread(ApiCall(request, callback))
    }

    fun getThreadByNowPlaying(callback:(Boolean, Response?) -> Unit): Thread{
        var request = "$_tmdbUrl/movie/now_playing?$_apiKey&language=$_language&region=$_region"
        return Thread(ApiCall(request, callback))
    }

    fun getThreadByUpcoming(callback:(Boolean, Response?) -> Unit): Thread{
        var request = "$_tmdbUrl/movie/upcoming?$_apiKey&language=$_language&region=$_region"
        return Thread(ApiCall(request, callback))
    }

    fun getThreadByOnTheAir(callback:(Boolean, Response?) -> Unit): Thread{
        var request = "$_tmdbUrl/tv/on_the_air?$_apiKey&language=$_language&region=$_region"
        return Thread(ApiCall(request, callback))
    }

    fun getThreadByAiringToday(callback:(Boolean, Response?) -> Unit): Thread{
        var request = "$_tmdbUrl/tv/airing_today?$_apiKey&language=$_language&region=$_region"
        return Thread(ApiCall(request, callback))
    }

    fun getTvBySearch(search:String, page: Int, callback:(Boolean, Response?) -> Unit){
        executor.execute(Thread(ApiCall("$_tmdbUrl/search/tv?$_apiKey&language=$_language&query=$search&region=$_region&page=$page", callback)))
    }

    fun getCategoriesBySearch(searches:ArrayList<String>, callbackForEach:(Boolean, Response?, String) -> Unit, callbackFinal: (Int) -> Unit){
        var batch: ArrayList<Thread> = ArrayList()
        searches.forEach {
            batch.add(Thread(ApiCallExtraInfo("$_tmdbUrl/search/movie?$_apiKey&language=$_language&query=$it&region=$_region", callbackForEach, it)))
        }
        executeBatch(batch, callbackFinal)
    }

    fun getTvEpisodesInBatch(info: SeasonEpisodeInfo, maxSeasons: Int, maxEpisodes:Int, callbackForEach:(Boolean, Response?) -> Unit, callbackFinal: (Int) -> Unit){
        var batch: ArrayList<Thread> = ArrayList()
        for (s in 1..maxSeasons){
            for (e in 1..maxEpisodes){
                batch.add(Thread(ApiCall("$_tmdbUrl/tv/${info.serieId}/season/$s/episode/$e?$_apiKey&language=$_language", callbackForEach)))
            }
        }
        executeBatch(batch, callbackFinal)
    }

    fun executeBatch(batch:ArrayList<Thread>, callback: (Int)->Unit){
        executor.execute {
            var futures = ArrayList<Future<*>>()
            batch.forEach {
                val future = executor.submit(it)
                futures.add(future)
            }
            futures.forEach {
                it.get()
            }
            callback(futures.size)
        }
    }

    companion object {
        private const val _tmdbPosterUrl = "https://image.tmdb.org/t/p/${SIZES.s342}"
        private const val _tmdbPosterUrlBig = "https://image.tmdb.org/t/p/${SIZES.s780}"
        fun getAbsoluteURL(url: String): String{
            var image = url
            if (url == "null") image = "/mJcTXKTsEGefD3rH3gnEW6qbpsu.jpg"
            return "$_tmdbPosterUrl$image"
        }
        fun getAbsoluteMainURL(url: String): String{
            var image = url
            if (url == "null") image = "/mJcTXKTsEGefD3rH3gnEW6qbpsu.jpg"
            return "$_tmdbPosterUrlBig$image"
        }
    }

    // Poster Sizes

    class SIZES{
        companion object {
            const val s92 = "w92" // Not used
            const val s154 = "w154"
            const val s185 = "w185"
            const val s342 = "w342"
            const val s500 = "w500"
            const val s780 = "w780"
            const val sOriginal = "original"
        }
    }
}