package dam.androidisrael.remembrit.provider.preferences

import android.content.Context
import android.content.SharedPreferences
import android.util.Base64
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey

class GlobalPreferences {

    companion object{
        private val pref_name = "R3M3MBR1T";
        fun writeString(context: Context, key: String, value: String){
            val sharedEn = getPreferences(context)
            with (sharedEn.edit()) {
                putString(key, value)
                apply()
            }
        }

        fun getString(context: Context, key: String): String? {
            val sharedEn = getPreferences(context)
            return sharedEn.getString(key, "")
        }

        private fun getPreferences(context: Context): SharedPreferences {
            val masterKeyAlias: MasterKey =
                MasterKey.Builder(context, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
                    .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
                    .build()
            return EncryptedSharedPreferences.create(
                context,
                pref_name,
                masterKeyAlias,
                EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            )
        }
    }

}