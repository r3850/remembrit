package dam.androidisrael.remembrit.model.session

import android.content.Context
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseUser
import dam.androidisrael.remembrit.provider.preferences.GlobalPreferences
import dam.androidisrael.remembrit.provider.services.firebase.FireB
import dam.androidisrael.remembrit.util.extension.isNull
import dam.androidisrael.remembrit.util.extension.toast

class UserSession {
    companion object{
        private var firebase = FireB()
        fun logIn(email:String, password:String, context: Context, onCompletedListener: (Task<AuthResult>)->Unit){
            firebase.logIn(email, password){
                if (it.isSuccessful){

                    saveUser(it.result.user!!, it.result.credential, context)
                }
                onCompletedListener(it)
            }
        }
        fun register(email:String, password:String, context: Context, onCompletedListener: (Task<AuthResult>)->Unit){
            firebase.register(email, password){
                onCompletedListener(it)
            }
        }
        private fun saveUser(user: FirebaseUser, credential: AuthCredential?, context: Context){
            GlobalPreferences.writeString(context, "uid", user.uid)
            if(!user.email.isNull()) GlobalPreferences.writeString(context, "email", user.email!!)
            GlobalPreferences.writeString(context, "credential", "")
            if(!credential.isNull()) user.reauthenticate(credential!!)
        }
        fun getUserUID(context: Context): String? {
            return GlobalPreferences.getString(context, "uid")
        }
        fun getUserEmail(context: Context): String? {
            return GlobalPreferences.getString(context, "email")
        }

        fun logOut(context: Context){
            GlobalPreferences.writeString(context, "uid", "")
            firebase.logOut()
        }
    }
}