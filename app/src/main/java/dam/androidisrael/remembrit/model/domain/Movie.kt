package dam.androidisrael.remembrit.model.domain

import java.io.Serializable
import java.time.LocalDate

data class Movie (var id: String, val title: String, var poster: String, var description: String, var release: LocalDate, var runtime: Int, var seasons:Int, var type: String, var tvInfo:SeasonEpisodeInfo?): Serializable {
    constructor(id: String, title: String, poster: String, description: String, release: LocalDate) : this(id, title, poster, description, release, 0)
    constructor(id: String, title: String, poster: String, description: String, release: LocalDate, runtime: Int): this(id, title, poster, description, release, runtime,0, "movie")
    constructor(id: String, title: String, poster: String, description: String, release: LocalDate, runtime: Int, seasons:Int, type: String): this(id, title, poster, description, release, runtime, seasons, type, null)
}