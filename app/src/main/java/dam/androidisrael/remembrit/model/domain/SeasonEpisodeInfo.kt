package dam.androidisrael.remembrit.model.domain

import java.io.Serializable

data class SeasonEpisodeInfo(var serieId:String, var seasonNumber: Int, var episodeNumber:Int): Serializable