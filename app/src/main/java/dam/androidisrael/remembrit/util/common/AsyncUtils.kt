package dam.androidisrael.remembrit.util.common

import dam.androidisrael.remembrit.provider.services.api.ApiCall
import java.util.concurrent.*
import kotlin.concurrent.thread

class AsyncUtils {
    companion object{
        private val _numberOfCores = Runtime.getRuntime().availableProcessors()
        private val workQueue: BlockingQueue<Runnable> = LinkedBlockingQueue<Runnable>()
        private val _keepAliveTime = 5L
        private val _keepAliveTimeUnit = TimeUnit.SECONDS
        val executor: ThreadPoolExecutor = ThreadPoolExecutor(
            _numberOfCores,
            _numberOfCores,
            _keepAliveTime,
            _keepAliveTimeUnit,
            workQueue
        )
    }
}
