package dam.androidisrael.remembrit.util.extension

fun Any?.isNull(): Boolean = this == null
