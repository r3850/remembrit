package dam.androidisrael.remembrit.util.extension

import android.app.Activity
import android.content.Context
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.FontRes
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import dam.androidisrael.remembrit.R

fun Activity.color(@ColorRes color:Int) = ContextCompat.getColor(this, color)
fun Activity.toast(text: String, length:Int = Toast.LENGTH_SHORT) = Toast.makeText(this, text, length).show()
fun Activity.alert(text: String): AlertDialog = AlertDialog.Builder(this).setMessage(text).show()
fun Activity.font(@FontRes font:Int) = ResourcesCompat.getFont(this, font)

fun Context.color(@ColorRes color:Int) = ContextCompat.getColor(this, color)
fun Context.alert(text: String): AlertDialog = AlertDialog.Builder(this).setMessage(text).show()
fun Context.toast(text: String, length:Int = Toast.LENGTH_SHORT) = Toast.makeText(this, text, length).show()
fun Context.font(@FontRes font:Int) = ResourcesCompat.getFont(this, font)