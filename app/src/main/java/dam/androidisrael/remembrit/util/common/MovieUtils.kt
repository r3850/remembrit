package dam.androidisrael.remembrit.util.common

import dam.androidisrael.remembrit.model.domain.Movie
import dam.androidisrael.remembrit.model.domain.SeasonEpisodeInfo
import dam.androidisrael.remembrit.util.extension.isNull
import org.json.JSONArray
import org.json.JSONObject
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class MovieUtils {
    companion object {
        private var formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        fun JsonMovieToMovie(json: String): Movie{
            var j :JSONObject = JSONObject(json)
            val movie: Movie = Movie(
                j.get("id").toString(),
                j.get("title").toString(),
                j.get("poster_path").toString(),
                j.get("overview").toString(),
                ToDateOrDefault(j.get("release_date").toString()),
                ToIntegerOrDefault(j.get("runtime").toString())
            )
            return movie
        }

        fun JsonTvToMovie(json: String): Movie{
            var j :JSONObject = JSONObject(json)
            val movie: Movie = Movie(
                j.get("id").toString(),
                j.get("name").toString(),
                j.get("poster_path").toString(),
                j.get("overview").toString(),
                ToDateOrDefault(j.get("first_air_date").toString()),
                0,
                ToIntegerOrDefault(j.get("number_of_seasons").toString()),
                "tv"
            )
            return movie
        }

        fun JsonSeasonToMovie(json: String, info: SeasonEpisodeInfo): Movie{
            var j :JSONObject = JSONObject(json)
            var episodes: JSONArray = j.get("episodes") as JSONArray
            val movie: Movie = Movie(
                j.get("id").toString(),
                j.get("name").toString(),
                j.get("poster_path").toString(),
                j.get("overview").toString(),
                ToDateOrDefault(j.get("air_date").toString()),
                0,
                episodes.length(),
                "season",
                info
            )
            return movie
        }

        fun JsonSeasonEpisodesToMovies(json: String, info: SeasonEpisodeInfo): ArrayList<Movie>{
            var episodes = ArrayList<Movie>()
            var j = JSONObject(json)
            var episodesJSON: JSONArray = j.get("episodes") as JSONArray
            for (i in 0 until episodesJSON.length()){
                var jsonEpisode = episodesJSON[i] as JSONObject
                info.episodeNumber = i + 1
                val movie: Movie = Movie(
                    jsonEpisode.get("id").toString(),
                    jsonEpisode.get("name").toString(),
                    jsonEpisode.get("still_path").toString(),
                    jsonEpisode.get("overview").toString(),
                    ToDateOrDefault(jsonEpisode.get("air_date").toString()),
                    0,
                    0,
                    "episode",
                    info
                )
                episodes.add(movie)
            }
            return episodes
        }

        fun JsonSeasonEpisodeToMovie(json: String, info:SeasonEpisodeInfo): Movie{
            var j :JSONObject = JSONObject(json)
            val movie: Movie = Movie(
                j.get("id").toString(),
                j.get("name").toString(),
                j.get("still_path").toString(),
                j.get("overview").toString(),
                ToDateOrDefault(j.get("air_date").toString()),
                0,
                0,
                "episode",
                info
            )
            return movie
        }

        fun JsonTvsToMovies(json: String): MutableList<Movie>{
            var movies : MutableList<Movie> = ArrayList()
            var j = JSONObject(json)
            var jList: JSONArray = j.get("results") as JSONArray
            var i = 0
            while (i < jList.length()){
                var jMovie = jList[i] as JSONObject
                var releaseDate: LocalDate =
                    if (jMovie.has("first_air_date")) ToDateOrDefault(jMovie.get("first_air_date").toString())
                    else LocalDate.of(1, 1, 1)
                movies.add(Movie(
                    jMovie.get("id").toString(),
                    jMovie.get("name").toString(),
                    jMovie.get("poster_path").toString(),
                    jMovie.get("overview").toString(),
                    releaseDate,
                    0,
                    0,
                    "tv"
                ))
                i++

            }
            return movies
        }

        fun JsonToMovies(json: String): MutableList<Movie>{
            var movies : MutableList<Movie> = ArrayList()
            var j = JSONObject(json)
            var jList: JSONArray = j.get("results") as JSONArray
            var i = 0
            while (i < jList.length()){
                var jMovie = jList[i] as JSONObject
                var releaseDate: LocalDate =
                    if (jMovie.has("release_date")) ToDateOrDefault(jMovie.get("release_date").toString())
                    else LocalDate.of(1, 1, 1)
                movies.add(Movie(
                    jMovie.get("id").toString(),
                    jMovie.get("title").toString(),
                    jMovie.get("poster_path").toString(),
                    jMovie.get("overview").toString(),
                    releaseDate
                ))
                i++

            }
            return movies
        }

        fun JsonCollectionToMovies(json: String): MutableList<Movie>{
            var movies : MutableList<Movie> = ArrayList()
            var j = JSONObject(json)
            var jList: JSONArray = j.get("parts") as JSONArray
            var i = 0
            while (i < jList.length()){
                var jMovie = jList[i] as JSONObject
                var releaseDate: LocalDate =
                    if (jMovie.has("release_date")) ToDateOrDefault(jMovie.get("release_date").toString())
                    else LocalDate.of(1, 1, 1)
                movies.add(Movie(
                    jMovie.get("id").toString(),
                    jMovie.get("title").toString(),
                    jMovie.get("poster_path").toString(),
                    jMovie.get("overview").toString(),
                    releaseDate
                ))
                i++

            }
            return movies
        }

        fun JsonGetPages(json: String): Int{
            var j = JSONObject(json)
            var pages: Int = Integer.parseInt(j.get("total_pages").toString())
            return pages
        }

        // Useful functions

        fun ToDateOrDefault(releaseDate: String): LocalDate{
            if (releaseDate.isNullOrBlank() || releaseDate == "null"){
                return LocalDate.of(1,1,1)
            }
            return LocalDate.parse(releaseDate, formatter)
        }

        fun ToIntegerOrDefault(number: String): Int{
            if (number.isNullOrBlank() || number == "null"){
                return 0
            }
            return Integer.parseInt(number)
        }
    }
}