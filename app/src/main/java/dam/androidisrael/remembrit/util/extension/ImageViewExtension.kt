package dam.androidisrael.remembrit.util.extension

import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.load(url: String) {
    if (url.isNotEmpty()){
        Glide.with(this.context).load(url).into(this)
    }
}