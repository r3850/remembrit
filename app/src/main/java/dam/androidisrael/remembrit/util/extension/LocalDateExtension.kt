package dam.androidisrael.remembrit.util.extension

import java.time.LocalDate
import java.time.MonthDay

fun LocalDate.toMonthDay() = MonthDay.of(this.month, this.dayOfMonth)